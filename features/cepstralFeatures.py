# -*- coding: utf-8 -*-
##########################################################################
# File name : cepstralFeatures.py
# Usages : Compute cepstrum
# Start date : 03 May 2016
# Last review : 13 July 2016
# Version : 0.1
# Author : Julien Vachaudez - julien.vachaudez@cerisic.be
# License : GNU AGPL v3 https://www.gnu.org/licenses/agpl-3.0.html
# Dependency  : Python 2.7
##########################################################################

import os
import numpy as np
import matplotlib.pyplot as plt

from pyautodiag.extract import csv2db as convert
from pyautodiag.core import processing as ps




#######################
# Function Definition #
#######################

def cepstg(xdata,ydata):
    """
    Parameters
    ----------
    param1 : type
        Description of parameter `param1`.
    param2 : type
        Description of parameter `param2`.

    Returns
    -------
    ret1 : type
        Description of the returned value

    Raises
    ------
    MyError:
        Description of my error
    """
    [x_spectA,y_spectA] = ps.fftg(xdata,ydata,window='rect')
    y_logA = np.log(np.abs(y_spectA))
    [x_cepstA,y_cepstA] = ps.fftg(x_spectA,y_logA,window='rect')


#    ceps = np.fft.ifft(np.log(np.abs(spectrum))).real
#    return ceps

    return x_cepstA,y_cepstA



def cepstv(xdata,ydata):
    """
    Parameters
    ----------
    param1 : type
        Description of parameter `param1`.
    param2 : type
        Description of parameter `param2`.

    Returns
    -------
    ret1 : type
        Description of the returned value

    Raises
    ------
    MyError:
        Description of my error
    """
    [x_spectV,y_spectV] = ps.fftv(xdata,ydata,window='rect')
    y_logV = np.log(np.abs(y_spectV))
    [x_cepstV,y_cepstV] = ps.fftv(x_spectV,y_logV,window='rect')


#    ceps = np.fft.ifft(np.log(np.abs(spectrum))).real
#    return ceps

    return x_cepstV,y_cepstV






def cepstrum2(xdata,ydata):
    """
    Parameters
    ----------
    param1 : type
        Description of parameter `param1`.
    param2 : type
        Description of parameter `param2`.

    Returns
    -------
    ret1 : type
        Description of the returned value

    Raises
    ------
    MyError:
        Description of my error
    """
    [x_spectA,y_spectA] = ps.fftg(xdata,ydata,window='rect')
    y_logA = np.log(y_spectA)
    y_cepstA = np.fft.ifft(y_logA)
    T = np.diff(x_spectA).mean()
    N = len(y_cepstA)
    x_cepstA = np.linspace(0.0, 1.0/(2.0*T), N/2,dtype=float)


    return x_cepstA,y_cepstA[0:N/2]




####################
# Class Definition #
####################

#class CepstralFeatures():
#    """
#    """
#    def cepstg(xdata,ydata):
#        """
#        Parameters
#        ----------
#        param1 : type
#            Description of parameter `param1`.
#        param2 : type
#            Description of parameter `param2`.
#    
#        Returns
#        -------
#        ret1 : type
#            Description of the returned value
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        [x_spectA,y_spectA] = ps.fftg(xdata,ydata,window='rect')
#        y_logA = np.log(np.abs(y_spectA))
#        [x_cepstA,y_cepstA] = ps.fftg(x_spectA,y_logA,window='rect')
#    
#    
#    #    ceps = np.fft.ifft(np.log(np.abs(spectrum))).real
#    #    return ceps
#    
#        return x_cepstA,y_cepstA
#    
#    
#    
#    def cepstv(xdata,ydata):
#        """
#        Parameters
#        ----------
#        param1 : type
#            Description of parameter `param1`.
#        param2 : type
#            Description of parameter `param2`.
#    
#        Returns
#        -------
#        ret1 : type
#            Description of the returned value
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        [x_spectV,y_spectV] = ps.fftv(xdata,ydata,window='rect')
#        y_logV = np.log(np.abs(y_spectV))
#        [x_cepstV,y_cepstV] = ps.fftv(x_spectV,y_logV,window='rect')
#    
#    
#    #    ceps = np.fft.ifft(np.log(np.abs(spectrum))).real
#    #    return ceps
#    
#        return x_cepstV,y_cepstV
#    
#    
#    
#    
#    
#    
#    def cepstrum2(xdata,ydata):
#        """
#        Parameters
#        ----------
#        param1 : type
#            Description of parameter `param1`.
#        param2 : type
#            Description of parameter `param2`.
#    
#        Returns
#        -------
#        ret1 : type
#            Description of the returned value
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        [x_spectA,y_spectA] = ps.fftg(xdata,ydata,window='rect')
#        y_logA = np.log(y_spectA)
#        y_cepstA = np.fft.ifft(y_logA)
#        T = np.diff(x_spectA).mean()
#        N = len(y_cepstA)
#        x_cepstA = np.linspace(0.0, 1.0/(2.0*T), N/2,dtype=float)
#    
#    
#        return x_cepstA,y_cepstA[0:N/2]







if __name__== "__main__":
    PATH = "C:\Users\Julien\Drive\Personal\AutoDiag\Code\pyautodiag\extract\WN27"
    FILENAME = 'WN27-GE27.04A-20150603-short.txt'
    FILE_PATH = os.path.join(PATH, FILENAME)
    
    [area,equipment,POM,speed,wave_meas] = convert.wave2lst(FILE_PATH,'AVV')
    
    xdata = [k[0] for k in wave_meas]
    ydata = [k[1] for k in wave_meas]
    
    [x_cepstA,y_cepstA] = cepstrum(xdata,ydata)
    plt.plot(x_cepstA,y_cepstA)