# -*- coding: utf-8 -*-
##########################################################################
# File name : spectralFeatures.py
# Usages : Extract spectral features
# Start date : 03 May 2016
# Last review : 25 September 2017
# Version : 1.0
# Author : Julien Vachaudez - julien.vachaudez@cerisic.be
# License : The pyautodiag project is distributed under the GNU Affero General Public 
#           License version 3 (https://www.gnu.org/licenses/agpl-3.0.html) and is also 
#           available under alternative licenses negotiated directly
#           with CERISIC.
# Dependency  : Python 2.7
##########################################################################

import numpy as np
import matplotlib.pyplot as plt
import json
import bisect
from pyautodiag.core import processing as ps
from pyautodiag.config.units import HERTZ,ORDERS,RPM

import unittest

#######################
# Function Definition #
#######################

def findIndex(a, x):
    """
    Parameters
    ----------
    a : 
        List in which the search must be done.

    x : 
        Boundary to search in list ``a´´

    Returns
    -------
    idx : int
        Index of the leftmost value greater than ``x´´
    """
    idx = bisect.bisect_left(a, x)
    
    return idx



def globalValue(x_spect,y_spect,freq=[(10,1000)],freq_unit="Hertz",speed=None,speed_unit="RPM"):
    """
    Parameters
    ----------
    x_spect : [Nx1] array
        Abscissa values of the spectrum given to compute the global value

    y_spect : [Nx1] array
        Ordinates values of the spectrum given to compute the global value

    freq : list of tuples
        Boundaries on which to compute the global value. Default value is "[(10,1000)]".

    freq_unit : str
        Unit of the given frequencies("Hertz" or "orders"). Default value is "Hertz".

    speed : float
        Rotating speed of the equipment.

    speed_unit : str
        Unit of the given speed ("RPM" or "Hertz"). Default value is "RPM"

    Returns
    -------
    GV : float
        Global value computed on the interval ``startFreq`` to ``endFreq``.
    """
    tmp = []
    try:
        assert isinstance(x_spect,(list,np.ndarray))
        assert isinstance(y_spect,(list,np.ndarray))
    except AssertionError:
        print("x_spect and y_spect msut be of type list or numpy.ndarray")
    
    try:
        assert len(x_spect) == len(y_spect)
    except AssertionError:
        print("x_spect and y_spect must have the same length")
    
    try:
        assert isinstance(freq,list)
        for x in freq:
            assert isinstance(x,tuple)
    except AssertionError:
        print("freq must be of type list and each element should be of type tuple")
    
    try:
        assert isinstance(freq_unit,str)
        assert freq_unit in HERTZ or freq_unit in ORDERS
    except AssertionError:
        print("freq_unit must be of type string AND in HERTZ or ORDERS")
    
    try:
        if speed !=None:
            assert isinstance(speed,(int,float))
    except AssertionError:
        print("speed must be of type int or float")
    
    try:
        assert isinstance(speed_unit,str)
        assert speed_unit in RPM or freq_unit in HERTZ
    except AssertionError:
        print("speed_unit must be of type string AND in RPM or HERTZ")
    
#    print "In SpectralFeatures()"
#    print "################"
#    print "Speed = ",speed, "of type", type(speed)
#    print "Speed Unit = ",speed_unit, "of type", type(speed_unit)
#    print "Freq = ",freq, "of type", type(freq)
#    print "Freq = ",freq[0], "of type", type(freq[0])
#    print "Freq Unit = ",freq_unit, "of type", type(freq_unit)
    
    if freq_unit in ORDERS:
        if speed != None:
            if speed_unit in RPM:
                speed_Hz = speed/60.0
                #speed_Hz = conv.rpm2hz(speed)
            elif speed_unit in HERTZ:
                speed_Hz = float(speed)
        else: # speed == None
            raise ValueError('The speed information is needed to convert orders in Hertz')
        
        # Convert ORDERS boundaries in HERTZ
        freq = [(x[0]*speed_Hz,x[1]*speed_Hz) for x in freq]
#        print "Freq = ",freq, "of type", type(freq)
        
    for i in range(len(freq)):
        startFreq = float(freq[i][0])
        endFreq = float(freq[i][1])
        
        startIdx = findIndex(x_spect,startFreq)
        endIdx = findIndex(x_spect,endFreq)
        
        tmp.extend(y_spect[startIdx:endIdx])
    
#    print "startIdx : ", startIdx
#    print "endIdx : ", endIdx
#    print tmp
#    print "Speed : ",speed
    
    GV = np.sqrt(np.sum(np.square(tmp)))
    
    return GV

#    
#    # IF freq_unit AND spect_unit have same units
#    if (((freq_unit in HERTZ) and (spect_unit in HERTZ)) or ((freq_unit in ORDERS) and (spect_unit in ORDERS))):
#        for i in range(len(freq)):
#            startFreq = float(freq[i][0])
#            endFreq = float(freq[i][1])
#            
#            startIdx = findIndex(x_spect,startFreq)
#            endIdx = findIndex(x_spect,endFreq)
#            
#            tmp.extend(y_spect[startIdx:endIdx])
#            
#    # IF freq_unit AND spect_unit have != units
#    elif (((freq_unit in HERTZ) and (spect_unit in ORDERS)) or ((freq_unit in ORDERS) and (spect_unit in HERTZ))):
#        # Converts speed in Hertz
#        if speed_unit in RPM:
#            speed_Hz = speed/60.0
#            speed_unit="Hz"
#        elif speed_unit in HERTZ:
#            speed_Hz = float(speed)
#        else:
#            print "The value of ``speed_unit´´ should be ``Hertz`` or ``RPM``"
#            return
#        
#        # Converts spectrum in orders
#        if spect_unit in HERTZ:
#            x_spect = x_spect/speed_Hz
#            spect_unit="ord"
#            tmp = globalValue(x_spect,y_spect,spect_unit=spect_unit,freq=freq,freq_unit=freq_unit,speed=speed_Hz,speed_unit=speed_unit)
#        
#        # Converts borders in orders
#        if freq_unit in HERTZ:
#            for i in range(len(freq)):
#                freq[i] = (freq[i][0]/speed_Hz,freq[i][1]/speed_Hz)
#                freq_unit="ord"
#            tmp = globalValue(x_spect,y_spect,spect_unit=spect_unit,freq=freq,freq_unit=freq_unit,speed=speed_Hz,speed_unit=speed_unit)
#    else:
#        print "The value of ``freq_unit´´  and ``spect_unit´´ should be ``Hertz`` or ``orders``"
#
#    GV = np.sqrt(np.sum(np.square(tmp)))
#    
#    return GV



##############
# Unit Tests #
##############

class SpectralFeaturesTest(unittest.TestCase):
    def setUp(self):
        with open(r'C:\1_CERISIC\AUTODIAG\Code\pyautodiag\data\I-Care\Preprod-154Ex-05032016-15h46-11.60mmsRMS-0.91Gpk.json') as json_data:
            data = json.load(json_data)
            json_data.close()
        
        self.xdata = [x[0] for x in data["g"]]
        self.ydata = [y[1] for y in data["g"]]
    
    def test_global_value(self):
        [x_spectV,y_spectV] = ps.fftv(self.xdata,self.ydata,wintype='rect')
        
        V_rms = globalValue(x_spectV,y_spectV,freq=[(10,1000)],freq_unit='Hz')
        self.assertLessEqual(V_rms, 11.68)
        
        V_rms = globalValue(x_spectV,y_spectV,freq=[(10,1000)],freq_unit='Hz',speed=80,speed_unit='Hertz')
        self.assertLessEqual(V_rms, 11.68)
        
        V_rms = globalValue(x_spectV,y_spectV,freq=[(0.95,1.05)],freq_unit='orders',speed=80,speed_unit='Hertz')
        self.assertLessEqual(V_rms, 11.68)
        
        self.assertRaises(ValueError, globalValue,x_spectV,y_spectV,freq=[(0.95,1.05)],freq_unit='orders')
        
        V_rms = globalValue(x_spectV,y_spectV,freq=[(0.95,1.05)],freq_unit='orders',speed=80,speed_unit='Hertz')
        self.assertLessEqual(V_rms, 11.68)

if __name__== "__main__":
    unittest.main()
    
    with open(r'C:\1_CERISIC\AUTODIAG\Code\pyautodiag\data\I-Care\Preprod-154Ex-05032016-15h46-11.60mmsRMS-0.91Gpk.json') as json_data:
        data = json.load(json_data)
        json_data.close()

    xdata = [x[0] for x in data["g"]]
    ydata = [y[1] for y in data["g"]]



    # Compute spectrum in orders
    [x_spectV,y_spectV] = ps.fftv(xdata,ydata,wintype='rect',speed_rpm=4800)
    # Spectrum in orders and limits in orders with speed
    V_rms = globalValue(x_spectV,y_spectV,freq=[(0.95,1.05),(0.95,1.05)],freq_unit='orders',speed=80,speed_unit='Hertz')
    print V_rms
    # Spectrum in orders and limits in orders with no speed
    V_rms = globalValue(x_spectV,y_spectV,freq=[(0.95,1.05)],freq_unit='orders')
    print V_rms
    # Spectrum in orders and limits in Hertz
    V_rms = globalValue(x_spectV,y_spectV,spect_unit="ord",freq=[(10,1000)],freq_unit='Hz',speed=4800,speed_unit='RPM')
    print V_rms



    # Compute spectrum in Hertz
    [x_spectV,y_spectV] = ps.fftv(xdata,ydata,wintype='rect')
    # Spectrum in Hertz and limits in Hertz
    V_rms = globalValue(x_spectV,y_spectV,spect_unit="Hz",freq=[(10,1000)],freq_unit='Hz')
    print V_rms
    # Spectrum in orders and limits in orders
    V_rms = globalValue(x_spectV,y_spectV,spect_unit="Hz",freq=[(0.95,1.05)],freq_unit='orders',speed=80,speed_unit='Hertz')
    print V_rms

    #plt.figure()
    #plt.plot([x[0] for x in data["fftv"]],[x[1] for x in data["fftv"]],'r')
    #plt.plot(x_spectV,y_spectV)
    #plt.show()