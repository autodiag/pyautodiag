# -*- coding: utf-8 -*-
############################################################################
# File name : timeFeatures.py
# Usages : Extract time features
# Start date : 21 April 2016
# Last review : 15 November 2016
# Version : 1.0
# Author : Julien Vachaudez - julien.vachaudez@cerisic.be
# License : The pyautodiag project is distributed under the GNU Affero General Public 
#           License version 3 (https://www.gnu.org/licenses/agpl-3.0.html) and is also 
#           available under alternative licenses negotiated directly
#           with CERISIC.
# Dependency  : Python 2.7
############################################################################


import numpy as np
from scipy import stats


#######################
# Function definition #
#######################

def mean(x):
    """
    Parameters
    ----------
    x : array
        Array containing the waveform of a POM.

    Returns
    -------
    np.mean(x) : float
        The mean value of the waveform

    Raises
    ------
    MyError:
        Description of my error
    """
    return np.mean(x)

def std(x):
    """
    Parameters
    ----------
    x : array
        Array containing the waveform of a POM.

    Returns
    -------
    np.std(x) : float
        The standard deviation value of the waveform

    Raises
    ------
    MyError:
        Description of my error
    """
    return np.std(x)

def max(x):
    """
    Parameters
    ----------
    x : array
        Array containing the waveform of a POM.

    Returns
    -------
    np.max(x) : float
        The maximum value of the waveform

    Raises
    ------
    MyError:
        Description of my error
    """
    return np.max(x)

def min(x):
    """
    Parameters
    ----------
    x : array
        Array containing the waveform of a POM.

    Returns
    -------
    np.min(x) : float
        The minimum value of the waveform

    Raises
    ------
    MyError:
        Description of my error
    """
    return np.min(x)

def rms(x):
    """
    Parameters
    ----------
    x : array
        Array containing the waveform of a POM.

    Returns
    -------
    rms : float
        The RMS (Root Mean Square) value of the waveform

    Raises
    ------
    MyError:
        Description of my error
    """
    rms = np.sqrt(np.mean(np.square(x)))
    return rms

def pk(x):
    """
    Parameters
    ----------
    x : array
        Array containing the waveform of a POM.

    Returns
    -------
    pk : float
        The peak (maximum) value of the waveform

    Raises
    ------
    MyError:
        Description of my error
    """
    pk = np.max(x)
    return pk

def pk2pk(x):
    """
    Parameters
    ----------
    x : array
        Array containing the waveform of a POM.

    Returns
    -------
    pk2pk : float
        The peak-to-peak value of the waveform

    Raises
    ------
    MyError:
        Description of my error
    """
    pk2pk = np.max(x) - np.min(x)
    return pk2pk

def moment(x,j):
    """
    Parameters
    ----------
    x : array
        Array containing the waveform of a POM.
    j : int
        Order of the moment.
    Returns
    -------
    mj : float
        The centered moment of order j

    Raises
    ------
    MyError:
        Description of my error
    """
    mj = np.sum((np.array(x)-np.mean(x))**j)/len(x)
    return mj

def kurtosis(x):
    """
    Parameters
    ----------
    x : array
        Array containing the waveform of a POM.

    Returns
    -------
    kurtosis : float
        The kurtosis value of the waveform.

    Raises
    ------
    MyError:
        Description of my error
    """
    kurtosis = moment(x,4)/moment(x,2)**2
    return kurtosis

def skewness(x):
    """
    Parameters
    ----------
    x : array
        Array containing the waveform of a POM.

    Returns
    -------
    skewness : float
        The skewness value of the waveform

    Raises
    ------
    MyError:
        Description of my error
    """
    skewness = moment(x,3)**2/moment(x,2)**3
    return skewness

def crest_factor(x):
    """
    Parameters
    ----------
    x : array
        Array containing the waveform of a POM.

    Returns
    -------
    crest : float
        The crest factor value of the waveform.

    Raises
    ------
    MyError:
        Description of my error
    """
    crest = pk(x)/rms(x)
    return crest

def clearance_factor(x):
    """
    Parameters
    ----------
    x : array
        Array containing the waveform of a POM.

    Returns
    -------
    clearance : float
        The clearance factor value of the waveform.

    Raises
    ------
    MyError:
        Description of my error
    """
    clearance = np.max(x)/(np.sum(np.sqrt(np.abs(x)))/len(x))**2
    return clearance

def shape_factor(x):
    """
    Parameters
    ----------
    x : array
        Array containing the waveform of a POM.

    Returns
    -------
    shape : float
        The shape factor value of the waveform.

    Raises
    ------
    MyError:
        Description of my error
    """
    shape = rms(x)/(np.sum(np.abs(x))/len(x))
    return shape

def impulse_factor(x):
    """
    Parameters
    ----------
    x : array
        Array containing the waveform of a POM.

    Returns
    -------
    impulse : float
        The impulse factor value of the waveform

    Raises
    ------
    MyError:
        Description of my error
    """
    impulse = np.max(x)/(np.sum(np.abs(x))/len(x))
    return impulse



def entropy(x,bins=10,entropy_range=None,qk=None,base=None):
    """
    Parameters
    ----------
    x : array
        Array containing the waveform of a POM.

    bins : int or sequence of scalars, optional
        If bins is an int, it defines the number of equal-width bins in the given range 
        (10, by default). If bins is a sequence, it defines the bin edges, including the 
        rightmost edge, allowing for non-uniform bin widths.

    range : (float, float), optional
        The lower and upper range of the bins. If not provided, range is simply (a.min(), a.max()). Values outside the range are ignored.

    qk : sequence, optional
        Sequence against which the relative entropy is computed. Should be in the same format as pk.

    base : float, optional
        The logarithmic base to use, defaults to ``e´´ (natural logarithm).

    Returns
    -------
    S : float
        The calculated entropy.

    Raises
    ------
    MyError:
        Description of my error
    """
    print "In timeFeatures"
    print type(entropy_range)
    
    hist, bin_edges = np.histogram(x,bins=bins,density=True)
    S = stats.entropy(hist,qk,base)
    return S


def multi_mean(x,frame_len=512,overlap=128):
    """
    Parameters
    ----------
    x : array
        Array containing the waveform of a POM.
    frame_len : integer
        Length of each frame. Length is specified in number of samples.
    overlap : integer
        Overlap between frames. Overlap is psecified in number of samples.
    Returns
    -------
    multimean : float
        The sum of the absolute value of the mean compute on each frames.
    """
    mean = 0
    
    nb_frame = len(x)/int(frame_len-overlap)
    for i in range(nb_frame):
        mean = mean + np.abs(np.mean(x[i*(frame_len-overlap):(i+1)*(frame_len-overlap)]))
    return mean




####################
# Class definition #
####################

#class TimeFeatures():
#    """
#    This class ...
#    
#    Parameters
#    ----------
#    """
#    def mean(self, x):
#        """
#        Parameters
#        ----------
#        x : array
#            Array containing the waveform of a POM.
#    
#        Returns
#        -------
#        np.mean(x) : float
#            The mean value of the waveform
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        return np.mean(x)
#    
#    def std(self, x):
#        """
#        Parameters
#        ----------
#        x : array
#            Array containing the waveform of a POM.
#    
#        Returns
#        -------
#        np.std(x) : float
#            The standard deviation value of the waveform
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        return np.std(x)
#    
#    def max(self, x):
#        """
#        Parameters
#        ----------
#        x : array
#            Array containing the waveform of a POM.
#    
#        Returns
#        -------
#        np.max(x) : float
#            The maximum value of the waveform
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        return np.max(x)
#    
#    def min(self, x):
#        """
#        Parameters
#        ----------
#        x : array
#            Array containing the waveform of a POM.
#    
#        Returns
#        -------
#        np.min(x) : float
#            The minimum value of the waveform
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        return np.min(x)
#    
#    def rms(self, x):
#        """
#        Parameters
#        ----------
#        x : array
#            Array containing the waveform of a POM.
#    
#        Returns
#        -------
#        rms : float
#            The RMS (Root Mean Square) value of the waveform
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        rms = np.sqrt(np.mean(np.square(x)))
#        return rms
#    
#    def pk(self, x):
#        """
#        Parameters
#        ----------
#        x : array
#            Array containing the waveform of a POM.
#    
#        Returns
#        -------
#        pk : float
#            The peak (maximum) value of the waveform
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        pk = np.max(x)
#        return pk
#    
#    def pk2pk(self, x):
#        """
#        Parameters
#        ----------
#        x : array
#            Array containing the waveform of a POM.
#    
#        Returns
#        -------
#        pk2pk : float
#            The peak-to-peak value of the waveform
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        pk2pk = np.max(x) - np.min(x)
#        return pk2pk
#    
#    def __moment(self, x,j):
#        """
#        Parameters
#        ----------
#        x : array
#            Array containing the waveform of a POM.
#        j : int
#            Order of the moment.
#        Returns
#        -------
#        mj : float
#            The centered moment of order j
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        mj = np.sum((np.array(x)-np.mean(x))**j)/len(x)
#        return mj
#        
#    def kurtosis(self, x):
#        """
#        Parameters
#        ----------
#        x : array
#            Array containing the waveform of a POM.
#    
#        Returns
#        -------
#        kurtosis : float
#            The kurtosis value of the waveform.
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        kurtosis = self.__moment(x,4)/self.__moment(x,2)**2
#        return kurtosis
#    
#    def skewness(self, x):
#        """
#        Parameters
#        ----------
#        x : array
#            Array containing the waveform of a POM.
#    
#        Returns
#        -------
#        skewness : float
#            The skewness value of the waveform
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        skewness = moment(x,3)**2/moment(x,2)**3
#        return skewness
#    
#    def crest_factor(self, x):
#        """
#        Parameters
#        ----------
#        x : array
#            Array containing the waveform of a POM.
#    
#        Returns
#        -------
#        crest : float
#            The crest factor value of the waveform.
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        crest = pk(x)/rms(x)
#        return crest
#    
#    def clearance_factor(self, x):
#        """
#        Parameters
#        ----------
#        x : array
#            Array containing the waveform of a POM.
#    
#        Returns
#        -------
#        clearance : float
#            The clearance factor value of the waveform.
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        clearance = np.max(x)/(np.sum(np.sqrt(np.abs(x)))/len(x))**2
#        return clearance
#    
#    def shape_factor(self, x):
#        """
#        Parameters
#        ----------
#        x : array
#            Array containing the waveform of a POM.
#    
#        Returns
#        -------
#        shape : float
#            The shape factor value of the waveform.
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        shape = rms(x)/(np.sum(np.abs(x))/len(x))
#        return shape
#    
#    def impulse_factor(self, x):
#        """
#        Parameters
#        ----------
#        x : array
#            Array containing the waveform of a POM.
#    
#        Returns
#        -------
#        impulse : float
#            The impulse factor value of the waveform
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        impulse = np.max(x)/(np.sum(np.abs(x))/len(x))
#        return impulse
#    
#    
#    
#    def entropy(self, x,bins=10,range=None,qk=None,base=None):
#        """
#        Parameters
#        ----------
#        x : array
#            Array containing the waveform of a POM.
#    
#        bins : int or sequence of scalars, optional
#            If bins is an int, it defines the number of equal-width bins in the given range 
#            (10, by default). If bins is a sequence, it defines the bin edges, including the 
#            rightmost edge, allowing for non-uniform bin widths.
#    
#        range : (float, float), optional
#            The lower and upper range of the bins. If not provided, range is simply (a.min(), a.max()). Values outside the range are ignored.
#    
#        qk : sequence, optional
#            Sequence against which the relative entropy is computed. Should be in the same format as pk.
#    
#        base : float, optional
#            The logarithmic base to use, defaults to ``e´´ (natural logarithm).
#    
#        Returns
#        -------
#        S : float
#            The calculated entropy.
#    
#        Raises
#        ------
#        MyError:
#            Description of my error
#        """
#        hist, bin_edges = np.histogram(x,bins,range,density=True)
#        S = stats.entropy(hist,qk,base)
#        return S
#    
#    
#    def multi_mean(self, x,frame_len=512,overlap=128):
#        """
#        Parameters
#        ----------
#        x : array
#            Array containing the waveform of a POM.
#        frame_len : integer
#            Length of each frame. Length is specified in number of samples.
#        overlap : integer
#            Overlap between frames. Overlap is psecified in number of samples.
#        Returns
#        -------
#        multimean : float
#            The sum of the absolute value of the mean compute on each frames.
#        """
#        mean = 0
#        
#        nb_frame = len(x)/int(frame_len-overlap)
#        for i in range(nb_frame):
#            mean = mean + np.abs(np.mean(x[i*(frame_len-overlap):(i+1)*(frame_len-overlap)]))
#        return mean

#class Measurement(TimeFeatures):
#    def __init__(self):
#        self.data = [0,3,4,1,2,3,0,2,1,3,2,0,2,2,3,2,5,2,3,999]
#    
##    def kurtosis(self, x):
##        kurtosis = TimeFeatures.kurtosis(self, x)
##        return kurtosis





if __name__== "__main__":
    
    meas = Measurement()
    #kurt = meas.kurtosis(meas.data)
    
    data = [0,3,4,1,2,3,0,2,1,3,2,0,2,2,3,2,5,2,3,999] # Kurtosis = 18.05
    
    data = [1,2,3,4]
    
    tf = TimeFeatures()
    tf.mean(data)
    tf.std(data)
    tf.max(data)
    tf.min(data)
    tf.rms(data)
    tf.pk(data)
    tf.pk2pk(data)
    tf.kurtosis(data)
    tf.skewness(data)
    tf.crest_factor(data)
    tf.clearance_factor(data)
    tf.shape_factor(data)
    tf.impulse_factor(data)
    
    
    print mean(data)
    print std(data)
    print max(data)
    print min(data)
    print rms(data)
    print pk(data)
    print pk2pk(data)
    print kurtosis(data)
    print skewness(data)
    print crest_factor(data)
    print clearance_factor(data)
    print shape_factor(data)
    print impulse_factor(data)