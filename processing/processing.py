# -*- coding: utf-8 -*-
##########################################################################
# File name : processing.py
# Usages : Tools library for processing the raw signal
# Start date : 22 June 2016
# Last review : 04 December 2017
# Version : 1.0
# Author : Julien Vachaudez - julien.vachaudez@cerisic.be
# License : The pyautodiag project is distributed under the GNU Affero General Public 
#           License version 3 (https://www.gnu.org/licenses/agpl-3.0.html) and is also 
#           available under alternative licenses negotiated directly
#           with CERISIC.
# Dependency : Python 2.7
##########################################################################

import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft
from scipy import signal
from scipy import integrate
from scipy.signal import filter_design as fd

from pyautodiag.utils import signal as sig
from pyautodiag.utils import plot

from pyautodiag.config.units import HERTZ,ORDERS


#######################
# Function Definition #
#######################

def window(N,wintype="rect"):
    """
    Parameters
    ----------
    N : int
        Length of the signal use.
    wintype : str
        Type of window to create. Default value is "rect".

    Returns
    -------
    w : array
        Array of size ``N ´´ containing the window.

    Raises
    ------
    MyError:
        Description of my error
    """
    if wintype == 'hanning' or wintype == 'hann':
        w = signal.hann(N)
    elif wintype == 'bartlett':
        w = signal.bartlett(N)
    elif wintype == 'blackman':
        w = signal.blackman(N)
    elif wintype == 'hamming':
        w = signal.hamming(N)
    elif wintype == 'parzen':
        w = signal.parzen(N)
    elif wintype == 'nuttall':
        w = signal.nuttall(N)
    elif wintype == 'boxcar' or wintype == 'rectangular' or wintype == 'rect':
        w = signal.boxcar(N)
    
    return w



def fftg(x_wave,y_wave,wintype='rect'):
    """
    Parameters
    ----------
    x_wave : array
        Abcsissa values of the waveform.
    y_wave : array
        Ordinates values of the waveform.
    
    Returns
    -------
    x_spect : array
         Abcsissa values of the acceleration spectrum.
    y_spect : array
         Ordinates values of the acceleration spectrum.
    
    Raises
    ------
    MyError:
        Description of my error
    """
    
    N = int(len(x_wave))
    T = np.diff(x_wave).mean()
    
    x_spect = np.linspace(0.0, 1.0/(2.0*T), N/2, dtype=float)
    
    w = window(N,wintype)
    
    ywf = fft(y_wave*w)
    y_spect = 2.0/N * np.abs(ywf[0:N/2])
    
    return [x_spect, y_spect]



def fftv(x_wave,y_wave,wintype='rect'):
    """
    Parameters
    ----------
    x_wave : array
        Abcsissa values of the waveform.
    y_wave : array
        Ordinates values of the waveform.

    Returns
    -------
    x_spect : array
         Abcsissa values of the velocity spectrum.
    y_spect : array
         Ordinates values of the velocity spectrum.

    Raises
    ------
    MyError:
        Description of my error
    """
    
    N = int(len(x_wave))
    T = np.diff(x_wave).mean()
    
    x_spect = np.linspace(0.0, 1.0/(2.0*T), N/2,dtype=float)
    
    w = window(N,wintype)
    
    ywf = fft(y_wave*w)
    y_spect = (9810 * (np.sqrt(2)/2))/(N *np.pi) * np.abs(ywf[0:N/2])
    
    x_spect[0] = 0
    y_spect = np.divide(y_spect[1:],x_spect[1:])
    y_spect = np.concatenate(([0],y_spect))
    
    return [x_spect, y_spect]




def cepstrum(x_wave,y_wave):
    """
    Parameters
    ----------
    x_wave : array
        Abcsissa values of the waveform.
    y_wave : array
        Ordinates values of the waveform.
    
    Returns
    -------
    x_cepst : array
         Abcsissa values of the cepstrum.
    y_cepst : array
         Ordinates values of the cepstrum.
    
    Raises
    ------
    MyError:
        Description of my error
    """
    
    def _unwrap(phase):
        samples = phase.shape[-1]
        unwrapped = np.unwrap(phase)
        center = (samples+1)//2
        if samples == 1: 
            center = 0  
        ndelay = np.array(np.round(unwrapped[...,center]/np.pi))
        unwrapped -= np.pi * ndelay[...,None] * np.arange(samples) / center
        return unwrapped, ndelay
        
    spectrum = np.fft.fft(y_wave)
    unwrapped_phase, ndelay = _unwrap(np.angle(spectrum))
    log_spectrum = np.log(np.abs(spectrum)) + 1j*unwrapped_phase
    y_cepst = np.fft.ifft(log_spectrum).real
    
    x_cepst = x_wave
    
    return x_cepst, y_cepst, ndelay






def g2v(x_waveA,y_waveA):
    """
    Parameters
    ----------
    x_waveA : array
        Abcsissa values of the waveform.
    y_waveA : array
        Ordinates values of the waveform.
    
    Returns
    -------
    x_waveV : array
         Abcsissa values of the velocity waveform.
    y_waveV : array
         Ordinates values of the velocity waveform.
    
    Raises
    ------
    MyError:
        Description of my error
    """
    
    T = np.diff(x_waveA).mean()
    Fs = 1/T
    
    x_waveV = x_waveA
    
    factor = 1000.0 * 9.81  # because 1(G)=9.81 (m/s^2). Req. : mm/s
    y_waveV = integrate.cumtrapz(y_waveA, dx=1.0/Fs) * factor
    y_waveV = np.insert(y_waveV, 0, 0)
    y_waveV = y_waveV-np.mean(y_waveV)

    return x_waveV,y_waveV





def highfilter(frequency):
    """
    High-pass filter coefficients for a given frequency.
    This is a static filter design.
    Warning: it is necessary to multiply the result by the proper amplitude.
    :param `numpy.Array` frequency: Frequency (in Hz)
    :returns: amplitude factor (to be multiplied by amplitude value)
    :rtype: `numpy.Array`
    """
    num = frequency ** 3
    den = (
        np.sqrt(frequency ** 2 + 0.5 ** 2) *
        np.sqrt(frequency ** 2 + 2.4 ** 2) *
        np.sqrt(frequency ** 2 + 1.6 ** 2)
    )
    return 1.0*num/(1.0*den)




def filtering(x_wave,y_wave,Wp,Ws,Rp,As,freq_unit="norm",speed_rpm=None,filter_type='butter',freqz=False):
    """
    Parameters
    ----------
    x_wave : array
        Abcsissa values of the waveform.
    y_wave : array
        Ordinates values of the waveform.
    Wp : float
        Passing frequency
    Ws : float
        Stop frequency
    Rp : float
        Maximum attenuation in pass band (in dB)
    As : float
        Minimum attenuation in stop band (in dB)
    filter_type : string
        Filter type ('butter', 'cheby1', 'cheby2', 'ellip'). Default value is "butter".
    freqz : bool
        If freqz is ``True´´ then the frequency response of the designed filter will be plotted.

    Returns
    -------
    x_wave : array
         Abcsissa values of the waveform.
    y_filter : array
         Ordinates values of the filtered waveform.

    Raises
    ------
    MyError:
        Description of my error
    """
    T = np.diff(x_wave).mean()
    Fs = 1.0/T
    
    # Converts order frequency in Hertz frequency
    if freq_unit in ORDERS:
        Wp = speed_rpm*np.array(Wp)/60.0
        Ws = speed_rpm*np.array(Ws)/60.0
        freq_unit = "Hertz"
    # Converts Hertz frequency in normalized frequency
    if freq_unit in HERTZ:
        Wp = 2.0*np.array(Wp)/Fs
        Ws = 2.0*np.array(Ws)/Fs

    # Compute the IIR filter and apply to the signal
    [b,a] = fd.iirdesign(Wp, Ws, Rp, As, ftype=filter_type)
    y_filter = signal.lfilter(b,a,y_wave)

    # Display frequency response of the filter, only if asked
    if freqz == True:
        w, H = signal.freqz(b,a)
        plt.figure()
        plt.plot(w/np.pi, 20*np.log10(np.abs(H)))
        plt.xlabel('Normalized Frequency [pi rad/sample]')
        plt.ylabel('Amplitude [dB]')

    return x_wave,y_filter



def demodulation(x_wave,y_wave):
    """
    Parameters
    ----------
    x_wave : array
        Abcsissa values of the waveform.
    y_wave : array
        Ordinates values of the waveform.

    Returns
    -------
    x_wave : array
         Abcsissa values of the waveform.
    y_demod : array
         Ordinates of the waveform enveloppe.
    """
    analytic_signal = signal.hilbert(y_wave)
    y_demod = np.abs(analytic_signal)
    
    return x_wave,y_demod


if __name__== "__main__":

    [x,y] = sig.createSine([5,3],[1,2],100,1000)
    plot.plotWaveform(x, y, "Samples", "Volatge (V)")

    [x_spectV,y_spectV] = fftv(x,y,window='rect',speed_rpm=4800)
