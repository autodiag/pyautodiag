# -*- coding: utf-8 -*-
##########################################################################
# File name : convert.py
# Usages : Utility script to convert units.
# Start date : 27 January 2018
# Last review : 27 January 2018
# Version : 0.1
# Author(s) : Julien Vachaudez - julien.vachaudez@cerisic.be
# License : The pyautodiag project is distributed under the GNU Affero General Public 
#           License version 3 (https://www.gnu.org/licenses/agpl-3.0.html) and is also 
#           available under alternative licenses negotiated directly
#           with CERISIC.
# Dependencies : Python 2.7
##########################################################################


def hz2rpm(data_hz):
    """
    Returns
    -------
    data_rpm : float
        Conversion of the input data in RPM.
    """
    data_rpm = data_hz*60.0
    
    return data_rpm


def rpm2hz(data_rpm):
    """
    Returns
    -------
    data_hz : float
        Conversion of the input data in Hz.
    """
    data_hz = data_rpm/60.0
    
    return data_hz


def in2mm(data_in):
    """
    Returns
    -------
    data_mm : float
        Conversion of the input data in millimeters.
    """
    data_mm = data_in * 25.4
    
    return data_mm


def mm2in(data_mm):
    """
    Returns
    -------
    data_in : float
        Conversion of the input data in inches.
    """
    data_in = data_mm/25.4
    
    return data_in


if __name__ == '__main__':
    
    print mm2in(25.4)
    print in2mm(1)
    print hz2rpm(50)
    print rpm2hz(3000)
