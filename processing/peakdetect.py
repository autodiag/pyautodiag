# -*- coding: utf-8 -*-
##########################################################################
# File name : peakdetect.py
# Usages : Tools library for processing the raw signal
# Start date : 22 June 2016
# Last review : 11 November 2016
# Version : 0.2
# Author : Julien Vachaudez - julien.vachaudez@cerisic.be
# License : GNU AGPL v3 https://www.gnu.org/licenses/agpl-3.0.html
# Dependency : Python 2.7
##########################################################################

import sys
from numpy import NaN, Inf, arange, isscalar, asarray, array
import numpy as np

def peakdet(x_wave,y_wave,delta):
    """
    Converted from MATLAB script at http://billauer.co.il/peakdet.html
    
    Returns two arrays
    
    function [maxtab, mintab]=peakdet(v, delta, x)
    %PEAKDET Detect peaks in a vector
    %        [MAXTAB, MINTAB] = PEAKDET(V, DELTA) finds the local
    %        maxima and minima ("peaks") in the vector V.
    %        MAXTAB and MINTAB consists of two columns. Column 1
    %        contains indices in V, and column 2 the found values.
    %      
    %        With [MAXTAB, MINTAB] = PEAKDET(V, DELTA, X) the indices
    %        in MAXTAB and MINTAB are replaced with the corresponding
    %        X-values.
    %
    %        A point is considered a maximum peak if it has the maximal
    %        value, and was preceded (to the left) by a value lower by
    %        DELTA.
    
    % Eli Billauer, 3.4.05 (Explicitly not copyrighted).
    % This function is released to the public domain; Any use is allowed.
    
    """
    maxtab = []
    mintab = []
       
    if x_wave is None:
        x_wave = arange(len(y_wave))
    
    y_wave = asarray(y_wave)
    
    if len(y_wave) != len(x_wave):
        sys.exit('Input vectors v and x must have same length')
    
    if not isscalar(delta):
        sys.exit('Input argument delta must be a scalar')
    
    if delta <= 0:
        sys.exit('Input argument delta must be positive')
    
    mn, mx = Inf, -Inf
    mnpos, mxpos = NaN, NaN
    
    lookformax = True
    
    for i in arange(len(y_wave)):
        this = y_wave[i]
        if this > mx:
            mx = this
            mxpos = x_wave[i]
        if this < mn:
            mn = this
            mnpos = x_wave[i]
        
        if lookformax:
            if this < mx-delta:
                maxtab.append((mxpos, mx))
                mn = this
                mnpos = x_wave[i]
                lookformax = False
        else:
            if this > mn+delta:
                mintab.append((mnpos, mn))
                mx = this
                mxpos = x_wave[i]
                lookformax = True

    return array(maxtab), array(mintab)



def enveloppe(x,y,delta=0.001,order=10):
    """
    """
    [envSup,envInf] = peakdet(x,y,delta)
    polySup = np.poly1d(np.polyfit(envSup[:,0], envSup[:,1],order))
    polyInf = np.poly1d(np.polyfit(envInf[:,0], envInf[:,1],order))
    
    return polySup,polyInf



if __name__=="__main__":
    from matplotlib.pyplot import plot, scatter, show
    series = [0,0,0,2,0,0,0,-2,0,0,0,2,0,0,0,-2,0]
    x = range(len(series))
    maxtab, mintab = peakdet(x,series,.3)
    plot(series)
    scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
    scatter(array(mintab)[:,0], array(mintab)[:,1], color='red')
    show()
