# -*- coding: utf-8 -*-
##########################################################################
# File name : dirs.py
# Usages : Description of the module
# Start date : 21 July 2017
# Last review : 18 Octobre 2023
# Version : 0.2
# Author(s) : Julien Vachaudez - vachaudezj@ceref.be
# License : The pyautodiag project is distributed under the GNU Affero General Public 
#           License version 3 (https://www.gnu.org/licenses/agpl-3.0.html) and is also 
#           available under alternative licenses negotiated directly
#           with CeREF.
# Dependencies : Python 3.8 or higher
##########################################################################

SPECIFICATIONS_DIR = r"./db/specifications/"
MEASUREMENTS_DIR = r"./pyautodiag/db/measurements/"
XL_PATH = r"./pyautodiagpyautodiag/files/xl"
GEN_REP_PATH = r'./pyautodiagpyautodiag/files/generated_rerorts'
PICKLE_PATH = r'./pyautodiag/files/pickles'