# -*- coding: utf-8 -*-
##########################################################################
# File name : faults.py
# Usages : Faults description
# Start date : 21 March 2017
# Last review : 04 April 2017
# Version : 0.2
# Author : Julien Vachaudez - julien.vachaudez@cerisic.be
# License : The pyautodiag project is distributed under the GNU Affero General Public 
#           License version 3 (https://www.gnu.org/licenses/agpl-3.0.html) and is also 
#           available under alternative licenses negotiated directly
#           with CERISIC.
# Dependency : Python 2.7
##########################################################################

FAULTS = {
"NO_FAULT":["Pas de défaut","Pas de problème"],
"IMBALANCE":["imbalance","déséquilibre"],
"MISALIGNMENT":[],
"LOOSENESS":["looseness","jeu"],
"BELT":["belt","courroie"],
"FTF":["ftf","cage"],
"BPFO":["bpfo","externe"],
"BPFI":["bpfi","interne"],
"BSF":["bsf","bille","éléments roulant"],
"RESONANCE":[],
"ELECTRIC":[],
"GEARBOX":[],
"CAVITATION":[],
"LUBRICATION":[],
"RUB":[]
}

FAULTS_LABELS = {
"NO_FAULT":0,
"IMBALANCE":1,
"MISALIGNMENT":2,
"LOOSENESS":3,
"BELT":4,
"FTF":5,
"BPFO":6,
"BPFI":7,
"BSF":8,
"RESONANCE":9,
"ELECTRIC":10,
"GEARBOX":11,
"CAVITATION":12,
"LUBRICATION":13,
"RUB":14
}