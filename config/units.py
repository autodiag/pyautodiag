# -*- coding: utf-8 -*-
##########################################################################
# File name : units.py
# Usages : Constant units types
# Start date : 07 November 2016
# Last review : 07 November 2016
# Version : 0.1
# Author : Julien Vachaudez - julien.vachaudez@cerisic.be
# License : The pyautodiag project is distributed under the GNU Affero General Public 
#           License version 3 (https://www.gnu.org/licenses/agpl-3.0.html) and is also 
#           available under alternative licenses negotiated directly
#           with CERISIC.
# Dependency : Python 2.7
##########################################################################

HERTZ = ["Hertz", "hertz", "Hz", "hz"]
ORDERS = ["Orders", "ORDERS", "order", "orders", "ord", "ORD", "ordre", "ordres", "Ordres",]
RPM = ["RPM", "rpm"]