# -*- coding: utf-8 -*-
##########################################################################
# File name : urgency.py
# Usages : Urgency description
# Start date : 03 April 2017
# Last review : 07 April 2017
# Version : 0.2
# Author : Julien Vachaudez - julien.vachaudez@cerisic.be
# License : The pyautodiag project is distributed under the GNU Affero General Public 
#           License version 3 (https://www.gnu.org/licenses/agpl-3.0.html) and is also 
#           available under alternative licenses negotiated directly
#           with CERISIC.
# Dependency : Python 2.7
##########################################################################

URGENCY = {
"NORMAL":["Pas de défaut"],
"LOW_ALERT":["Admissible"],
"HIGH_ALERT":["Défaut prononcé"],
"LOW_ALARM":["Action proactiv"],
"HIGH_ALARM":["Action correcti"]
}

URGENCY_LABELS = {
"NORMAL":0,
"LOW_ALERT":1,
"HIGH_ALERT":2,
"LOW_ALARM":3,
"HIGH_ALARM":4
}