# -*- coding: utf-8 -*-
##########################################################################
# File name : belts.py
# Usages : Description of the module
# Start date : Thu Nov 09 09:35:11 2017
# Last review : Thu Nov 09 09:35:11 2017
# Version : 0.1
# Author(s) : Julien Vachaudez - julien.vachaudez@cerisic.be
# License : The pyautodiag project is distributed under the GNU Affero General Public 
#           License version 3 (https://www.gnu.org/licenses/agpl-3.0.html) and is also 
#           available under alternative licenses negotiated directly
#           with CERISIC.
# Dependencies : Python 2.7
##########################################################################

import os
import sys

class Belts():
    """
    """
    def __init__(self,*args):
        
        self.belts = list()
        cursor = queryDatabase("pyautodiag","belts",*args)
        
        for doc in cursor:
            self.belts.append(Belt(doc))
        
    def numBelts(self):
        print len(self.belts)


class Belt:
    """
    """
    def __init__(self,doc):
            self.ID       = doc['ID']
            self.manufacturer = doc['manufacturer']
            self.type     = doc['type']
            self.freq     = doc['freq']
            self.speedout = doc['speedout']
            self.length   = doc['length']
            self.sheave1  = doc['sheave1']
            self.sheave2  = doc['sheave2']

if __name__ == '__main__':
    
