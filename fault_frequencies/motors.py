# -*- coding: utf-8 -*-
##########################################################################
# File name : motors.py
# Usages : Description of the module
# Start date : Thu Nov 09 09:34:12 2017
# Last review : Thu Nov 09 09:34:12 2017
# Version : 0.1
# Author(s) : Julien Vachaudez - julien.vachaudez@cerisic.be
# License : The pyautodiag project is distributed under the GNU Affero General Public 
#           License version 3 (https://www.gnu.org/licenses/agpl-3.0.html) and is also 
#           available under alternative licenses negotiated directly
#           with CERISIC.
# Dependencies : Python 2.7
##########################################################################

import os
import sys

class Motors():
    """
    """
    def __init__(self,*args):
        
        self.motors = list()
        cursor = queryDatabase("pyautodiag","motors",*args)
        
        for doc in cursor:
            self.motors.append(Motor(doc))
        
    def numMotors(self):
        print len(self.motors)


class Motor:
    """
    """
    def __init__(self,doc):
            self.ID    = doc['ID']
            self.manufacturer = doc['manufacturer']
            self.model = doc['model']
            self.frame = doc['frame']
            self.RPM   = doc['RPM']
            self.power = doc['power']
            self.volts = doc['volts']
            self.amps  = doc['amps']
            self.type  = doc['type']
            self.poles = doc['poles']
            self.bars  = doc['bars']
            self.slots = doc['slots']

if __name__ == '__main__':
    
