# -*- coding: utf-8 -*-
##########################################################################
# File name : bearings.py
# Usages : Description of the module
# Start date : Thu Nov 09 09:35:48 2017
# Last review : 13/10/2023
# Version : 0.2
# Author(s) : Julien Vachaudez - vachaudezj@ceref.be
# License : The pyautodiag project is distributed under the GNU Affero General Public 
#           License version 3 (https://www.gnu.org/licenses/agpl-3.0.html) and is also 
#           available under alternative licenses negotiated directly
#           with CeREF.
# Dependencies : Python 3.8
##########################################################################

import os
import sys
import pandas as pd
#from pymongo import MongoClient

class Bearings():
    """
    """
    def __init__(self,*args):
        
        self.bearings = list()
        cursor = queryDatabase("pyautodiag","bearings",*args)
        
        for doc in cursor:
            self.bearings.append(Bearing(doc))
        
    def numBearings(self):
        print(len(self.bearings))
    
    def getBearings(self,idx):
        print(self.bearings[idx])
        return self.bearings[idx]


class Bearing():
    """
    """
    def __init__(self,doc):
            self.manufacturer = doc['manufacturer']
            self.ID   = doc['ID']
            self.FTF  = doc['FTF']
            self.BPFI = doc['BPFI']
            self.BPFO = doc['BPFO']
            self.BSF  = doc['BSF']
            self.type = doc['type']
            self.nb_balls = doc['nb_balls']

def brg2pandas(FILE_PATH):
    """
    Parameters
    ----------
    FILE_PATH : str
        String containing the path to the bearing database text file.

    Returns
    -------
        Return a DatFrame containing the bearing database.
    """
    # Create de bearings dataframe
    bearings = pd.DataFrame(columns=["manufacturer", "type", "nb_balls", "FTF", "BSF", "BPFO", "BPFI"])

    skipCount = 0
    
    with open(FILE_PATH) as f:
        for line in f:
            skipCount = skipCount + 1
            if (skipCount > 5):
                currentLine1 = line[0:34].split('  ')
                currentLine2 = line[34:].split('  ')
                newLine1 = [x.strip() for x in currentLine1 if x]
                newLine2 = [x.strip() for x in currentLine2 if x]

                new_brg = {"manufacturer": newLine1[1][0:3],
                           "type": ' '.join(newLine1[1:]).strip()[4:],
                           "nb_balls": int(newLine2[0]),
                           "FTF":  float(newLine2[1]),
                           "BSF":  float(newLine2[2]),
                           "BPFO": float(newLine2[3]),
                           "BPFI": float(newLine2[4])
                           }
                bearings = bearings.append(new_brg, ignore_index=True)
    return bearings




def brg2db(FILE_PATH):
    """
    Parameters
    ----------
    FILE_PATH : str
        String containing the path to the bearing database text file.

    Returns
    -------
        Populate the MongoDB bearing database.

    Raises
    ------
    MyError:
        Description of my error
    """
    client = MongoClient()
    db = client['bearings']
    collection = db['bearings']
    
    brg = db.bearings
    
    skipCount = 0
    
    with open(FILE_PATH) as f:
        for line in f:
            skipCount = skipCount + 1
            if (skipCount > 5):
                currentLine1 = line[0:34].split('  ')
                currentLine2 = line[34:].split('  ')
                newLine1 = [x.strip() for x in currentLine1 if x]
                newLine2 = [x.strip() for x in currentLine2 if x]

                new_brg = {"manufacturer": newLine1[1][0:3],
                           "type": ' '.join(newLine1[1:]).strip()[4:],
                           "nb_balls": int(newLine2[0]),
                           "FTF":  float(newLine2[1]),
                           "BSF":  float(newLine2[2]),
                           "BPFO": float(newLine2[3]),
                           "BPFI": float(newLine2[4])
                           }
                brg.insert_one(new_brg)
                
                
def queryBearing(manufacturerList, typeList):
    """
    Parameters
    ----------
    manufacturerList : list
        [1xN] list containing the manufacturers trigrams to search in the database.
    typeList : list
        [1xN] list containing the manufacturers trigrams to search in the database.

    Returns
    -------
    faultfreq : list
        [4xN] list containing the fault frequencies for the queried bearings\
        in the database

    Raises
    ------
    MyError:
        Description of my error
    """
    client = MongoClient()
    db = client['bearings']
    faultfreq = [[],[],[],[]]
    if (len(manufacturerList) == len(typeList)):
        for i in range(len(manufacturerList)):
            cursor = db.bearings.find({"manufacturer":manufacturerList[i],"type":typeList[i]})
            for brg in cursor:
                faultfreq[0].append(brg['FTF'])
                faultfreq[1].append(brg['BSF'])
                faultfreq[2].append(brg['BPFI'])
                faultfreq[3].append(brg['BPFO'])
    else:
        print("The manufacturer list and the bearing type list must have the\
        same length")
    return faultfreq


if __name__ == '__main__':
    print("Executing main program")
    bearings_db = brg2pandas(r"/home/jvachaudez/OneDrive/HELHa/2MA_TraitementSignal/Cours/master/Labs/Project/pyautodiag/db/fault_frequencies/Bearings.txt")
    print("Stop")
    print(bearings_db.head())