# -*- coding: utf-8 -*-
##########################################################################
# File name : autodiag.py
# Usages : Description of the module
# Start date : 22 November 2017
# Last review : 29 November 2017
# Version : 0.1
# Author(s) : Julien Vachaudez - julien.vachaudez@cerisic.be
# License : The pyautodiag project is distributed under the GNU Affero General Public 
#           License version 3 (https://www.gnu.org/licenses/agpl-3.0.html) and is also 
#           available under alternative licenses negotiated directly
#           with CERISIC.
# Dependencies : Python 2.7
##########################################################################

import pyautodiag.config.options as options

class Autodiag():
    """
    """
    def __init__(self):
        print "Welcome in AUTODIAG"
        
        self.__measurements_options = options.measurements_options
        self.__mapping_options = options.mapping_options
        self.__processing_options = options.processing_options[0]
        
        self.options = dict()
        
        self.set_options(mapping = "max")
        self.set_options(filter_data = False)
        self.set_options(processing = "by_report")
        self.set_options(measurements = "getAllMeasurements")
        
        self.print_options()
    
    
    def __set_options(self, **kwargs):
        """
        Set object options.
        
        Parameters
        ----------
        mapping : str
            Description.
        processing : str
            Description.
        measurements : str
            Description.
        filter_data : bool
            Description.
        filter_spec : dict
            Description.
        """
        
        if "mapping" in kwargs:
            if kwargs.get("mapping") in self.mapping_options:
                self.options["mapping"] = kwargs.get("mapping")
            else:
                print "Invalid option"
        
        elif "processing" in kwargs:
            if kwargs.get("processing") in self.processing_options:
                self.options["processing"] = kwargs.get("processing")
            else:
                print "Invalid option"
        
        elif "measurements" in kwargs:
            if kwargs.get("measurements") in self.measurements_options:
                self.options["measurements"] = kwargs.get("measurements")
            else:
                print "Invalid option"
        
        elif "filter_data" in kwargs:
            self.options["filter_data"] = kwargs.get("filter_data")
        
        elif "filter_spec" in kwargs:
            filter_spec = kwargs.get("filter_spec")
            
            self.options["filter_spec"] = dict()
            
            try:
                self.options["filter_spec"]["Wp"] = filter_spec["Wp"]
                self.options["filter_spec"]["Ws"] = filter_spec["Ws"]
                self.options["filter_spec"]["Rp"] = filter_spec["Rp"]
                self.options["filter_spec"]["As"] = filter_spec["As"]
                self.options["filter_spec"]["freq_unit"] = filter_spec["freq_unit"]
                self.options["filter_spec"]["type"] = filter_spec["type"]
            except:
                print "Wrong filter_spec option"
        
        else:
            print "Invalid option"
    
    
    def print_options(self):
        """
        Print object options.
        """
        print "mapping function: ",  self.options["mapping"]
        print "processing method: ", self.options["processing"]
        print "query measurement: ", self.options["measurements"]
        print "filter_data: ",       self.options["filter_data"]
        print "filter_spec: ",       self.options["filter_spec"]





if __name__ == '__main__':
    print "INSERT TESTS"