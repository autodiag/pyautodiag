# -*- coding: utf-8 -*-
############################################################################
# File name : plot.py
# Usages : Create one lines plots.
# Start date : 23 June 2016
# Last review : 11 November 2016
# Version : 1.0
# Author : Julien Vachaudez - julien.vachaudez@cerisic.be
# License : GNU AGPL v3 https://www.gnu.org/licenses/agpl-3.0.html
# Dependency  : Python 2.7
############################################################################

import matplotlib.pyplot as mplt
import numpy as np


def pyplot(xdata,ydata,color='b',figNum=None,xlabel=None,ylabel=None,label=None,grid=True):
    """
    Parameters
    ----------
    xdata : list of list
        List of numpy arrays containing all the `xdata` of every plot.
    ydata : list of list
        List of numpy arrays containing all the `ydata` of every plot.
    color : string
        String containing the display color of the figures. Default is 'b'.
    figNum : integer
        Integer describing on which figure the data must be plotted.
    xlabel : list of strings
        List of strings, containing the `xlabel` of every plot.
    ylabel : list of strings
        List of strings, containing the `ylabel` of every plot.
    title : string
        String containing the title of the figure.
    label : list of strings
        List of strings, containing the `label` of every plot.
    grid : bool
        Boolean. If `True` a grid is displayed, if `False` not. Default is `True`
    """
    mplt.figure(figNum)
    mplt.plot(xdata,ydata,color,label=label)
    if xlabel != None:
        mplt.xlabel(xlabel)
    if ylabel != None:
        mplt.ylabel(ylabel)
    if label != None:
        mplt.legend()
    if grid == True:
        mplt.grid()
    mplt.show()


def pyharm(freq,nb_harm,window,heigth,color='r',figNum=None,label=None):
    """
    Parameters
    ----------
    freq : float
        Float containing the foncdamental frequency.
    nb_harm : int
        Integer representing the number of harmonics to display.
    window : float
        Float representing the width of the computation window.
    heigth : float
        Float representing the heigth of the .
    color : string
        String containing the display color of the figures. Default is 'b'.
    figNum : integer
        Integer describing on which figure the data must be plotted.
    label : list of strings
        List of strings, containing the `label` of every plot.
    """
    mplt.figure(figNum)
    for i in np.arange(1,nb_harm+1,1):
        if i == 1:
            mplt.plot([(i*freq-window/2,i*freq-window/2),(i*freq-window/2,i*freq+window/2),(i*freq+window/2,i*freq+window/2)],[(0,heigth),(heigth,heigth),(heigth,0)],color,label=label)
        else:
            mplt.plot([(i*freq-window/2,i*freq-window/2),(i*freq-window/2,i*freq+window/2),(i*freq+window/2,i*freq+window/2)],[(0,heigth),(heigth,heigth),(heigth,0)],color)
    if label != None:
        mplt.legend()
    mplt.show()


def pyscatter(xdata,ydata,color='b',figNum=None,xlabel=None,ylabel=None,label=None,grid=True):
    """
    Parameters
    ----------
    xdata : list of list
        List of numpy arrays containing all the `xdata` of every plot.
    ydata : list of list
        List of numpy arrays containing all the `ydata` of every plot.
    color : string
        String containing the display color of the figures. Default is 'b'.
    figNum : integer
        Integer describing on which figure the data must be plotted.
    xlabel : list of strings
        List of strings, containing the `xlabel` of every plot.
    ylabel : list of strings
        List of strings, containing the `ylabel` of every plot.
    title : string
        String containing the title of the figure.
    label : list of strings
        List of strings, containing the `label` of every plot.
    grid : bool
        Boolean. If `True` a grid is displayed, if `False` not. Default is `True`
    """
    mplt.figure(figNum)
    mplt.scatter(xdata,ydata,c=color)
    if xlabel != None:
        mplt.xlabel(xlabel)
    if ylabel != None:
        mplt.ylabel(ylabel)
    if label != None:
        mplt.legend()
    if grid == True:
        mplt.grid()
    mplt.show()




def pysubplot(xdata,ydata,subplot,color='b',xlabel=None,ylabel=None,title=None,label=None,grid=True):
    """
    Parameters
    ----------
    xdata : list of list
        List of numpy arrays containing all the `xdata` of every plot.
    ydata : list of list
        List of numpy arrays containing all the `ydata` of every plot.
    subplot : tuple
        Tuple containing the number of row and column in the image.
    color : string
        String containing the display color of the figures. Default is 'b'.
    xlabel : list of strings
        List of strings, containing the `xlabel` of every plot.
    ylabel : list of strings
        List of strings, containing the `ylabel` of every plot.
    title : string
        String containing the title of the figure.
    label : list of strings
        List of strings, containing the `label` of every plot.
    grid : bool
        Boolean. If `True` a grid is displayed, if `False` not. Default is `True`

    Raises
    ------
    MyError:
        Description of my error
    """
    mplt.figure()
    mplt.title(title)
    for i in range(subplot[0]):
        for j in range(subplot[1]):
            #print subplot[0],subplot[1],(subplot[1]*i+j+1)
            mplt.subplot(subplot[0],subplot[1],(subplot[1]*i+j+1))
            if label != None:
                mplt.plot(xdata[subplot[1]*i +j],ydata[subplot[1]*i+j],color,label=label[subplot[1]*i +j])
            else:
                mplt.plot(xdata[subplot[1]*i +j],ydata[subplot[1]*i+j],color,label=label)
        if xlabel != None:
            mplt.xlabel(xlabel[subplot[1]*i +j])
        if ylabel != None:
            mplt.ylabel(ylabel[subplot[1]*i +j])
        if label != None:
            mplt.legend()
        if grid == True:
            mplt.grid()
    mplt.show()