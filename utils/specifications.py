# -*- coding: utf-8 -*-
##########################################################################
# File name : specifications.py
# Usages : Tools library for processing the raw signal
# Start date : 22 June 2016
# Last review : 18 October 2023
# Version : 1.1
# Author : Julien Vachaudez - vachaudezj@ceref.be
# License : The pyautodiag project is distributed under the GNU Affero General Public 
#           License version 3 (https://www.gnu.org/licenses/agpl-3.0.html) and is also 
#           available under alternative licenses negotiated directly
#           with CeREF.
# Dependency : Python 3.8 or higher
##########################################################################

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.fftpack import fft
from scipy import signal
from scipy import integrate
from scipy.signal import filter_design as fd

#from pyautodiag.utils import signal as sig
#from pyautodiag.utils import plot

#from pyautodiag.config.units import HERTZ,ORDERS


def brg2pandas(FILE_PATH):
    """
    Parameters
    ----------
    FILE_PATH : str
        String containing the path to the bearing database text file.

    Returns
    -------
        Return a DataFrame containing the bearing database.
    """
    # Create de bearings dataframe
    bearings = pd.DataFrame(columns=["manufacturer", "type", "nb_balls", "FTF", "BSF", "BPFO", "BPFI"])

    skipCount = 0
    
    with open(FILE_PATH) as f:
        for line in f:
            skipCount = skipCount + 1
            if (skipCount > 5):
                currentLine1 = line[0:34].split('  ')
                currentLine2 = line[34:].split('  ')
                newLine1 = [x.strip() for x in currentLine1 if x]
                newLine2 = [x.strip() for x in currentLine2 if x]

                new_brg = {"manufacturer": newLine1[1][0:3],
                           "type": ' '.join(newLine1[1:]).strip()[4:],
                           "nb_balls": int(newLine2[0]),
                           "FTF":  float(newLine2[1]),
                           "BSF":  float(newLine2[2]),
                           "BPFO": float(newLine2[3]),
                           "BPFI": float(newLine2[4])
                           }
                bearings = bearings.append(new_brg, ignore_index=True)
    return bearings



def belt2pandas(FILE_PATH):
    """
    Parameters
    ----------
    FILE_PATH : str
        String containing the path to the belt database text file.
    
    Returns
    -------
        Return a DataFrame containing the belt database.
    """
    # Create de bearings dataframe
    belts = pd.DataFrame(columns=["ID","manufacturer", "type", "freq", "speedout", "sheave1", "sheave2", "length"])
    
    skipCount = 0
    
    with open(FILE_PATH) as f:
        for line in f:
            skipCount = skipCount + 1
            if (skipCount > 5):
                currentLine1 = line[0:32]
                currentLine2 = line[32:].split(' ')
                newLine1 = currentLine1
                newLine2 = [x.strip() for x in currentLine2 if x]
                
                new_belt = {"ID":int(newLine1[0:8].strip()),
                            "manufacturer":newLine1[8:15].strip(),
                            "type": newLine1[15:].strip(),
                            "freq": float(newLine2[0]),
                            "speedout":  float(newLine2[1]),
                            "sheave1":  float(newLine2[2]),
                            "sheave2": float(newLine2[3]),
                            "length": float(newLine2[4])
                           }
                belts = belts.append(new_belt, ignore_index=True)
    return belts


def motor2pandas(FILE_PATH):
    """
    Parameters
    ----------
    FILE_PATH : str
        String containing the path to the motor database text file.
    
    Returns
    -------
        Return a DataFrame containing the motor database.
    """
    # Create de bearings dataframe
    motors = pd.DataFrame(columns=["ID","manufacturer","model","frame","RPM","power","volts","amps","type","poles","bars","slots"])
    
    skipCount = 0
    
    with open(FILE_PATH) as f:
        for line in f:
            skipCount = skipCount + 1
            if (skipCount > 5):
                currentLine1 = line[0:34]
                currentLine2 = line[34:].split(' ')
                newLine1 = currentLine1
                newLine2 = [x.strip() for x in currentLine2 if x]
                
                new_motor = {"ID":int(newLine1[0:7].strip()),
                            "manufacturer":newLine1[7:11].strip(),
                            "model": newLine1[11:24].strip(),
                            "frame": newLine1[24:].strip(),
                            "RPM": int(newLine2[0]),
                            "power":  float(newLine2[1]),
                            "volts":  float(newLine2[2]),
                            "amps": float(newLine2[3]),
                            "type": newLine2[4],
                            "poles": int(newLine2[5]),
                            "bars": int(newLine2[6]),
                            "slots": int(newLine2[7])
                           }
                motors = motors.append(new_motor, ignore_index=True)
    return motors




def brg2db(FILE_PATH):
    """
    Parameters
    ----------
    FILE_PATH : str
        String containing the path to the bearing database text file.
    
    Returns
    -------
        Populate the MongoDB bearing database.
    """
    client = MongoClient()
    db = client['pyautodiag']
    
    brg = db.bearings
    
    skipCount = 0
    
    with open(FILE_PATH) as f:
        for line in f:
            skipCount = skipCount + 1
            if (skipCount > 5):
                currentLine1 = line[0:33]
                currentLine2 = line[33:].split(' ')
                newLine1 = currentLine1
                newLine2 = [x.strip() for x in currentLine2 if x]
                
                new_brg = {"ID": int(newLine1[0:11].strip()),
                           "manufacturer": newLine1[11:18].strip(),
                           "type": newLine1[18:].strip(),
                           "nb_balls": int(newLine2[0]),
                           "FTF":  float(newLine2[1]),
                           "BSF":  float(newLine2[2]),
                           "BPFO": float(newLine2[3]),
                           "BPFI": float(newLine2[4])
                           }
                brg.insert_one(new_brg)


def belt2db(FILE_PATH):
    """
    Parameters
    ----------
    FILE_PATH : str
        String containing the path to the belt database text file.
    
    Returns
    -------
        Populate the MongoDB belt database.
    """
    client = MongoClient()
    db = client['pyautodiag']
    
    belt = db.belts
    
    skipCount = 0
    
    with open(FILE_PATH) as f:
        for line in f:
            skipCount = skipCount + 1
            if (skipCount > 5):
                currentLine1 = line[0:32]
                currentLine2 = line[32:].split(' ')
                newLine1 = currentLine1
                newLine2 = [x.strip() for x in currentLine2 if x]
                
                new_belt = {"ID":int(newLine1[0:8].strip()),
                            "manufacturer":newLine1[8:15].strip(),
                            "type": newLine1[15:].strip(),
                            "freq": float(newLine2[0]),
                            "speedout":  float(newLine2[1]),
                            "sheave1":  float(newLine2[2]),
                            "sheave2": float(newLine2[3]),
                            "length": float(newLine2[4])
                           }
                belt.insert_one(new_belt)


def motor2db(FILE_PATH):
    """
    Parameters
    ----------
    FILE_PATH : str
        String containing the path to the motor database text file.
    
    Returns
    -------
        Populate the MongoDB motor database.
    """
    client = MongoClient()
    db = client['pyautodiag']
    
    motor = db.motors
    
    skipCount = 0
    
    with open(FILE_PATH) as f:
        for line in f:
            skipCount = skipCount + 1
            if (skipCount > 5):
                currentLine1 = line[0:34]
                currentLine2 = line[34:].split(' ')
                newLine1 = currentLine1
                newLine2 = [x.strip() for x in currentLine2 if x]
                
                new_motor = {"ID":int(newLine1[0:7].strip()),
                            "manufacturer":newLine1[7:11].strip(),
                            "model": newLine1[11:24].strip(),
                            "frame": newLine1[24:].strip(),
                            "RPM": int(newLine2[0]),
                            "power":  float(newLine2[1]),
                            "volts":  float(newLine2[2]),
                            "amps": float(newLine2[3]),
                            "type": newLine2[4],
                            "poles": int(newLine2[5]),
                            "bars": int(newLine2[6]),
                            "slots": int(newLine2[7])
                           }
                motor.insert_one(new_motor)
                
                
                

def queryBearing(manufacturerList, typeList):
    """
    Parameters
    ----------
    manufacturerList : list
        [1xN] list containing the manufacturers trigrams to search in the database.
    typeList : list
        [1xN] list containing the manufacturers trigrams to search in the database.

    Returns
    -------
    faultfreq : list
        [4xN] list containing the fault frequencies for the queried bearings\
        in the database

    Raises
    ------
    MyError:
        Description of my error
    """
    client = MongoClient()
    db = client['bearings']
    faultfreq = [[],[],[],[]]
    if (len(manufacturerList) == len(typeList)):
        for i in range(len(manufacturerList)):
            cursor = db.bearings.find({"manufacturer":manufacturerList[i],"type":typeList[i]})
            for brg in cursor:
                faultfreq[0].append(brg['FTF'])
                faultfreq[1].append(brg['BSF'])
                faultfreq[2].append(brg['BPFI'])
                faultfreq[3].append(brg['BPFO'])
    else:
        print("The manufacturer list and the bearing type list must have the\
        same length")
    return faultfreq






if __name__== "__main__":
    pass
    #[x,y] = sig.createSine([5,3],[1,2],100,1000)
    #plot.plotWaveform(x, y, "Samples", "Volatge (V)")

    #[x_spectV,y_spectV] = fftv(x,y,window='rect',speed_rpm=4800)
