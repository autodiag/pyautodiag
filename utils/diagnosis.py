# -*- coding: utf-8 -*-
##########################################################################
# File name : diagnosis.py
# Usages : Description of the module
# Start date : 17 November 2017
# Last review : 08 January 2018
# Version : 0.2
# Author(s) : Julien Vachaudez - julien.vachaudez@cerisic.be
# License : The pyautodiag project is distributed under the GNU Affero General Public 
#           License version 3 (https://www.gnu.org/licenses/agpl-3.0.html) and is also 
#           available under alternative licenses negotiated directly
#           with CERISIC.
# Dependencies : Python 2.7
##########################################################################

import os
import json
import pickle
from bson import json_util

from pyautodiag.core.db import txt2db as db
from pyautodiag.core.main.measurements import Measurements
from pyautodiag.core.main.measurements import Measurement
#from pyautodiag.core.ml.ml import MachineLearning
from pyautodiag.core.ml.ml import SupervizedLearning
from pyautodiag.core.ml.ml import UnsupervizedLearning

from pyautodiag.config.dirs import PICKLE_PATH


class Diagnosis(Measurements):
    """
    """
    def __init__(self):
        self.measurements = list()
        self.diagnose     = list()
        self.labels       = list()
        self.features     = list()
        self.prediction   = None
        self.pickle_name = ""
    
    def load_measurements_folder(self, FOLDER_PATH):
        """
        Parameters
        ----------
        FOLDER_PATH : str
            String containing the path to the measurements text folder.
        
        Returns
        -------
            Append **Measurements** to measurements list/
        """
        for txt_file in os.listdir(FOLDER_PATH):
            self.load_measurement_file(os.path.join(FOLDER_PATH,txt_file))
    
    def load_measurement_file(self, FILE_PATH):
        """
        Parameters
        ----------
        FILE_PATH : str
            String containing the path to the measurement text file.
        
        Returns
        -------
            Append **Measurement** to measurements list.
        """
        meas = db.load_measurement(FILE_PATH, output='json')
        meas = json.loads(meas, object_hook=json_util.object_hook)
        
        # Append Measurement to measurements list
        self.measurements.append(Measurement(meas))
    
    def load_pickle(self, name):
        """
        Parameters
        ----------
        name : str
            String containing the name of the pickle to load.
            
        Returns
        -------
            Load the pickle as the default classifier.
        """
        pickle_name = name + ".pickle"
        FULL_PATH = PICKLE_PATH + "\\" + pickle_name
        try:
            classifier = open(FULL_PATH,'rb')
            
            self.pickle_name = name
            self.clf = pickle.load(classifier)
        except:
            print "Pickle not found"
    
    def score(self):
        """
        Returns the mean accuracy on the test data and labels.
        """
        if self.clf:
            accuracy = self.clf.score(self.features, self.labels)
            print("Score: ", accuracy)
        else:
            print "Classificator is not yet defined"
    
    def predict(self):
        """
        Perform classification on samples in X.
        """
        if self.clf:
            self.prediction = self.clf.predict(self.features)
        else:
            print "Classificator is not yet defined"
    
    def add_labeled_features(self, labels, *args):
        """
        """
        sl = SupervizedLearning()
        sl.add_to_features(labels, *args)
        
        # Extend **labels** and **features** lists
        self.labels.extend(sl.labels)
        self.features.extend(sl.features)
        
        # Delete unused object
        del sl
    
    def add_features(self, *args):
        """
        """
        usl = UnsupervizedLearning()
        usl.add_to_features(*args)
        
        # Extend **features** list
        self.features.extend(usl.features)
        
        # Delete unused object
        del usl
    
    def print_prediction(self, probability=False):
        """
        """
        if self.prediction is not None:
            print "Area \t Equipment \t\t POM \t Prediction \t Probility (%)"
            print "-----------------------------------------------------"
            for measurement in self.measurements:
                meas_idx = self.measurements.index(measurement)
                
                if len(measurement.equipment) < 15:
                    equip = measurement.equipment + "\t\t"
                else:
                    equip = measurement.equipment + "\t"
                
                print measurement.area + "\t" +\
                      equip +\
                      measurement.POM + "\t" +\
                      str(self.prediction[meas_idx]) + "\t" +\
                      str(self.clf.predict_proba(self.features[meas_idx][0])[0][int(self.prediction[meas_idx])]*100)
        else:
            print "There is no prediction yet"
    def print_pickle_name(self):
        """
        """
        print "Loaded pickle: ", self.pickle_name
    
    def imbalance_diagnosis(self):
        print "Front"


if __name__ == '__main__':
    
    
    FOLDER_PATH = r'C:\1_CERISIC\AUTODIAG\Code\pyautodiag\data\AUTODIAG\GSK\WN10\EC10.12_ZONE 1'
    FILE_PATH = r'C:\1_CERISIC\AUTODIAG\Code\pyautodiag\data\AUTODIAG\GSK\WN10\EC10.12_ZONE 1\AVA-20150130-10h15.txt'
    
    diag = Diagnosis()
    diag.load_measurements_folder(FOLDER_PATH)
    
    diag.load_measurement_file(FILE_PATH)
    
    print "DONE"