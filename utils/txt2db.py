# -*- coding: utf-8 -*-
############################################################################
# File name : txt2db.py
# Usages : Convert MHM text files in a usable database
# Start date : 15 April 2016
# Last review : 08 January 2018
# Version : 1.3
# Author : Julien Vachaudez - julien.vachaudez@cerisic.be
# License : The pyautodiag project is distributed under the GNU Affero General Public 
#           License version 3 (https://www.gnu.org/licenses/agpl-3.0.html) and is also 
#           available under alternative licenses negotiated directly
#           with CERISIC.
# Dependency  : Python 2.7, MongoDB
############################################################################

import os
from pymongo import MongoClient
import glob
import datetime
import numpy as np
import json
import logging
from bson import json_util


def buildStruct():
    client = MongoClient()
    db = client.pyautodiag
    
    struct = dict()
    
    num_customer = db.measurements.distinct("client")
    
    for i in range(len(num_customer)):
        num_area = db.measurements.distinct("area",{"client":num_customer[i]})
        struct[num_customer[i]] = dict()
        
        for j in range(len(num_area)):
            num_equipment = db.measurements.distinct("equipment", {"client":num_customer[i],"area":num_area[j]})
            struct[num_customer[i]][num_area[j]] = dict()
            
            for k in range(len(num_equipment)):
                num_pom = db.measurements.distinct("POM", {"client":num_customer[i],"area":num_area[j],"equipment":num_equipment[k]})
                struct[num_customer[i]][num_area[j]][num_equipment[k]] = dict()
                
                for l in range(len(num_pom)):
                    num_date = db.measurements.distinct("date", {"client":num_customer[i],"area":num_area[j],"equipment":num_equipment[k],"POM":num_pom[l]})
                    struct[num_customer[i]][num_area[j]][num_equipment[k]][num_pom[l]] = dict()
                    
                    for m in range(len(num_date)):
                        struct[num_customer[i]][num_area[j]][num_equipment[k]][num_pom[l]][num_date[m]] = num_date[m]
    
    collection = db['struct']
    collection.insert(struct,check_keys=False)
    return struct


#cwd = r'C:\1_CERISIC\AUTODIAG\Code\pyautodiag\core\db'
logging.basicConfig(filename=cwd+'\db.log',format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG)

def brg2db(FILE_PATH):
    """
    Parameters
    ----------
    FILE_PATH : str
        String containing the path to the bearing database text file.
    
    Returns
    -------
        Populate the MongoDB bearing database.
    """
    client = MongoClient()
    db = client['pyautodiag']
    
    brg = db.bearings
    
    skipCount = 0
    
    with open(FILE_PATH) as f:
        for line in f:
            skipCount = skipCount + 1
            if (skipCount > 5):
                currentLine1 = line[0:33]
                currentLine2 = line[33:].split(' ')
                newLine1 = currentLine1
                newLine2 = [x.strip() for x in currentLine2 if x]
                
                new_brg = {"ID": int(newLine1[0:11].strip()),
                           "manufacturer": newLine1[11:18].strip(),
                           "type": newLine1[18:].strip(),
                           "nb_balls": int(newLine2[0]),
                           "FTF":  float(newLine2[1]),
                           "BSF":  float(newLine2[2]),
                           "BPFO": float(newLine2[3]),
                           "BPFI": float(newLine2[4])
                           }
                brg.insert_one(new_brg)


def belt2db(FILE_PATH):
    """
    Parameters
    ----------
    FILE_PATH : str
        String containing the path to the belt database text file.
    
    Returns
    -------
        Populate the MongoDB belt database.
    """
    client = MongoClient()
    db = client['pyautodiag']
    
    belt = db.belts
    
    skipCount = 0
    
    with open(FILE_PATH) as f:
        for line in f:
            skipCount = skipCount + 1
            if (skipCount > 5):
                currentLine1 = line[0:32]
                currentLine2 = line[32:].split(' ')
                newLine1 = currentLine1
                newLine2 = [x.strip() for x in currentLine2 if x]
                
                new_belt = {"ID":int(newLine1[0:8].strip()),
                            "manufacturer":newLine1[8:15].strip(),
                            "type": newLine1[15:].strip(),
                            "freq": float(newLine2[0]),
                            "speedout":  float(newLine2[1]),
                            "sheave1":  float(newLine2[2]),
                            "sheave2": float(newLine2[3]),
                            "length": float(newLine2[4])
                           }
                belt.insert_one(new_belt)


def motor2db(FILE_PATH):
    """
    Parameters
    ----------
    FILE_PATH : str
        String containing the path to the motor database text file.
    
    Returns
    -------
        Populate the MongoDB motor database.
    """
    client = MongoClient()
    db = client['pyautodiag']
    
    motor = db.motors
    
    skipCount = 0
    
    with open(FILE_PATH) as f:
        for line in f:
            skipCount = skipCount + 1
            if (skipCount > 5):
                currentLine1 = line[0:34]
                currentLine2 = line[34:].split(' ')
                newLine1 = currentLine1
                newLine2 = [x.strip() for x in currentLine2 if x]
                
                new_motor = {"ID":int(newLine1[0:7].strip()),
                            "manufacturer":newLine1[7:11].strip(),
                            "model": newLine1[11:24].strip(),
                            "frame": newLine1[24:].strip(),
                            "RPM": int(newLine2[0]),
                            "power":  float(newLine2[1]),
                            "volts":  float(newLine2[2]),
                            "amps": float(newLine2[3]),
                            "type": newLine2[4],
                            "poles": int(newLine2[5]),
                            "bars": int(newLine2[6]),
                            "slots": int(newLine2[7])
                           }
                motor.insert_one(new_motor)


def load_measurement(FILE_PATH, output='db'):
#def txt2db(FILE_PATH, output):
    """
    Parameters
    ----------
    FILE_PATH : str
        String containing the path to the measurements database text file.
    
    Returns
    -------
        Populate the MongoDB measurements database.
    """
    # Connect to MongoDB
    client = MongoClient()
    db = client['pyautodiag']
    measure = db.measurements
    
    tmp_data_x = []
    data_y = []
    with open(FILE_PATH) as f:
        for line in f:
            
            if (line.strip() != ""):
                
                if (line.strip().split()[0]=="Area:"):
                    customer = line.strip().split("[")[2].split("-")[0].strip()
                    area = line.strip().split("[")[1].split("]")[0].strip()
                    #area = line.strip().split()[3][1:]
                
                if (line.strip().split()[0]=="Equipment:"):
                    equipment = line.strip().split('[')[2][:-1].strip()
                
                if (line.strip().split()[0]=="Measurement"):
                    POM = line.strip().split('[')[1][0:3]
                
                if (line.strip().split()[0]=="Date/Time:"):
                    DAY = int(line.strip().split(' ')[1].split("/")[0])
                    MONTH = int(line.strip().split(' ')[1].split("/")[1])
                    YEAR = int("20" + line.strip().split(' ')[1].split("/")[2])
                    HOUR = int(line.strip().split(' ')[3].split(":")[0])
                    MIN = int(line.strip().split(' ')[3].split(":")[1])
                    SEC = int(line.strip().split(' ')[3].split(":")[2])
                
                if (line.strip().split()[0]=="Speed:"):
                    speed = float(line.strip().split(' ')[1])
                    speed_unit = line.strip().split(' ')[2]
                
                if (line.strip().split()[0]=="LOAD:"):
                    load = float(line.strip().split(' ')[1])
                
                if (line.strip().split()[0]=="Number"):
                    nb_samples = int(line.strip().split()[3])
                
                if (line.strip().split()[0]=="Time"):
                    meas_unit = line.strip().split()[2]
                    # Skip 2 line
                    line = f.next()
                    line = f.next()
                    for j in range(nb_samples):
                        tmp_data_x.append(float(line.split()[0]))
                        data_y.append(float(line.split()[1]))
                        line = f.next()
                    
                    start = tmp_data_x[0]
                    step = np.mean(np.diff(tmp_data_x))
                    stop = step*nb_samples
                    
                    data_x = (start,stop,step)
                    
                    # Create JSON object and insert in MongoDB
                    new_meas = {"client": customer,
                                "area": area,
                                "equipment": equipment,
                                "POM": POM,
                                "date": datetime.datetime(YEAR,MONTH,DAY,HOUR,MIN,SEC),
                                "speed": speed,
                                "speed_unit": speed_unit,
                                "load":load,
                                "meas_unit": meas_unit,
                                "data_x":data_x,
                                "data_y":data_y,
                                "nb_samples":nb_samples
                               }
                    if output == 'json':
                        return json.dumps(new_meas, default=json_util.default)
                    else:
                        measure.insert_one(new_meas)
            else:
                pass


#def txt2json(FILE_PATH):
#    """
#    Parameters
#    ----------
#    FILE_PATH : str
#        String containing the path to the measurements database text file.
#
#    Returns
#    -------
#        Populate the MongoDB measurements database.
#    """
#    
#    tmp_data_x = []
#    data_y = []
#    with open(FILE_PATH) as f:
#        for line in f:
#            
#            if (line.strip() != ""):
#                
#                if (line.strip().split()[0]=="Area:"):
#                    customer = line.strip().split("[")[2].split("-")[0].strip()
#                    area = line.strip().split("[")[1].split("]")[0].strip()
#                    #area = line.strip().split()[3][1:]
#                    
#                if (line.strip().split()[0]=="Equipment:"):
#                    equipment = line.strip().split('[')[2][:-1].strip()
#                    
#                if (line.strip().split()[0]=="Measurement"):
#                    POM = line.strip().split('[')[1][0:3]
#                    
#                if (line.strip().split()[0]=="Date/Time:"):
#                    DAY = int(line.strip().split(' ')[1].split("/")[0])
#                    MONTH = int(line.strip().split(' ')[1].split("/")[1])
#                    YEAR = int("20" + line.strip().split(' ')[1].split("/")[2])
#                    HOUR = int(line.strip().split(' ')[3].split(":")[0])
#                    MIN = int(line.strip().split(' ')[3].split(":")[1])
#                    SEC = int(line.strip().split(' ')[3].split(":")[2])
#                    
#                if (line.strip().split()[0]=="Speed:"):
#                    speed = float(line.strip().split(' ')[1])
#                    speed_unit = line.strip().split(' ')[2]
#                    
#                if (line.strip().split()[0]=="LOAD:"):
#                    load = float(line.strip().split(' ')[1])
#                    
#                if (line.strip().split()[0]=="Number"):
#                    nb_samples = int(line.strip().split()[3])
#                    
#                if (line.strip().split()[0]=="Time"):
#                    meas_unit = line.strip().split()[2]
#                    # Skip 2 line
#                    line = f.next()
#                    line = f.next()
#                    for j in range(nb_samples):
#                        tmp_data_x.append(float(line.split()[0]))
#                        data_y.append(float(line.split()[1]))
#                        line = f.next()
#                    
#                    start = tmp_data_x[0]
#                    step = np.mean(np.diff(tmp_data_x))
#                    stop = step*nb_samples
#                    
#                    data_x = (start,stop,step)
#                        
#                    # Create JSON object
#                    new_meas = {"customer": customer,
#                                "area": area,
#                                "equipment": equipment,
#                                "POM": POM,
#                                "date": datetime.datetime(YEAR,MONTH,DAY,HOUR,MIN,SEC),
#                                "speed": speed,
#                                "speed_unit": speed_unit,
#                                "load":load,
#                                "meas_unit": meas_unit,
#                                "data_x":data_x,
#                                "data_y":data_y,
#                                "nb_samples":nb_samples
#                               }
#            else:
#                pass
#    return json.dumps(new_meas)


def folder2db(FOLDER_PATH):
    """
    Parameters
    ----------
    FOLDER_PATH : str
        String containing the path to the measurements database text folder.
    
    Returns
    -------
        Populate the MongoDB measurements database.
    """
    counter = 0
    size = len(glob.glob(FOLDER_PATH))
    
    for i in glob.glob(FOLDER_PATH):
        load_measurement(i)
        
        counter = counter + 1
        print "Importing measurement "+ str(counter) + "/" + str(size)
        logging.info(str(counter) + "/" + str(size) + " : " + str(i))


def struct2lst(PATH):
    """
    """
    
    MONTHS = ["janv.","fev.","mars","avr.","mai","juin","juil.","aout","sept.","oct.","nov.","dec."]
    MONTHS_DEC = ["01","02","03","04","05","06","07","08","09","10","11","12"]
    POMS = ["VV","VA","VP","HV","HA","HP","AV","AA","AP"]
#    POMS_OLDER = ["NH","NV","NP","AH","AV","AP"]
    
    database = []
    client = []
    area = []
    equipments = []
    POMs,tmp_POMs = [],[]
    dates,tmp_dates_lev1,tmp_dates_lev2 = [],[],[]
    
    with open(PATH) as f:
        for line in f.readlines():
            #Find Database
            if "Database:" in line and ":" in line:
                database.append(line.split(":")[1].split(".")[0].strip())
            #Find Area
            if "Area:" in line and ":" in line:
                client.append(line.split(":")[1].split("-")[0].strip())
                area.append(line.split(":")[1].split("-")[1].strip())
            # Find Equipments
            if "Equipment" in line and ":" in line:
                equipments.append(line.split(":")[1].strip())
                POMs.append(tmp_POMs)
                tmp_POMs = []
                tmp_dates_lev1.append(tmp_dates_lev2)
                tmp_dates_lev2 = []
                dates.append(tmp_dates_lev1)
                tmp_dates_lev1 = []
            # Find POMs
            try:
                if line.split("-")[1].strip()[1:] in POMS:
                    tmp_POMs.append(line.split("-")[1].strip())
                    tmp_dates_lev1.append(tmp_dates_lev2)
                    tmp_dates_lev2 = []
            except:
                print "Index out of range"
                
            # Find Dates
            try:
                if "-" in line:
                    if line.strip().split("-")[1] in MONTHS and line.split(":")[0].strip() != "Report Date":
                        DAY = int(line.split()[0].split("-")[0])
                        MONTH = int(MONTHS_DEC[MONTHS.index(line.split()[0].split("-")[1])])
                        YEAR = int("20" + line.split()[0].split("-")[2])
                        HOUR = int(line.split()[1].split(":")[0])
                        MIN = int(line.split()[1].split(":")[1])
                        
                        tmp_dates_lev2.append(datetime.datetime(YEAR,MONTH,DAY,HOUR,MIN,0,0))
                
                if "/" in line:
                    if line.strip().split("/")[1] in MONTHS_DEC and line.split(":")[0].strip() != "Report Date":
                        DAY = int(line.strip().split("/")[0])
                        MONTH = int(line.split()[0].split("/")[1])
                        YEAR = int("20" + line.split()[0].split("/")[2])
                        HOUR = int(line.split()[1].split(":")[0])
                        MIN = int(line.split()[1].split(":")[1])
                        
                        tmp_dates_lev2.append(datetime.datetime(YEAR,MONTH,DAY,HOUR,MIN,0,0))
            
            except IndexError:
                print "Index out of range"
    POMs.append(tmp_POMs)
    tmp_dates_lev1.append(tmp_dates_lev2)
    dates.append(tmp_dates_lev1)
    dates.pop(0)
    POMs.pop(0)
    for i in range(len(dates)):
        dates[i].pop(0)
    
    return client,area,equipments,POMs,dates


def lst2db(client,area,equipments,POMs,dates):
    """
    ##############################
    #Converts lists to JSON object
    ##############################
    #dates = [[["20160130", "20160130"],["20150130", "20150130"],["20140130", "20140130"]],[["20160130", "20160130"],["20150130", "20150130"],["20140130", "20140130"]]]
    #POMs = [["AVV","AVA","AVP"],["AVV","AVA","AVP"]]
    #equipments = ["GP10.01_ZONE 1","GE10.01A_ZONE 1"]
    #client = ["GSK"]
    #area = ["WN48"]
    """
    
    data = {}
    data['client'] = client[0]
    data['area'] = area[0]
    
    data['equipments'] = dict()
    for j in range(len(POMs)):
        data['equipments'][equipments[j]] = dict()
        data['equipments'][equipments[j]]['POMs'] = dict()
        for i in range(len(POMs[j])):
            data['equipments'][equipments[j]]['POMs'][POMs[j][i]] = dict()
            data['equipments'][equipments[j]]['POMs'][POMs[j][i]]['dates'] = dates[j][i]
    
    client = MongoClient()
    db = client['pyautodiag']
    collection = db['struct']
    collection.insert(data,check_keys=False)
    
    return data


def equip2db(customer,area,equipments,POMs,dates):
    """
    ##############################
    #Converts lists to JSON object
    ##############################
    #dates = [[["20160130", "20160130"],["20150130", "20150130"],["20140130", "20140130"]],[["20160130", "20160130"],["20150130", "20150130"],["20140130", "20140130"]]]
    #POMs = [["AVV","AVA","AVP"],["AVV","AVA","AVP"]]
    #equipments = ["GP10.01_ZONE 1","GE10.01A_ZONE 1"]
    #client = ["GSK"]
    #area = ["WN48"]
    """
    
    for j in range(len(equipments)):
        data = dict()
        data['customer'] = customer[0]
        data['area'] = area[0]
        data['bearings'] = list()
        data['belts'] = list()
        data['equipment'] = equipments[j]
        data['POMs'] = dict()
        for i in range(len(POMs[j])):
            data['POMs'][POMs[j][i]] = dict()
            data['POMs'][POMs[j][i]]['dates'] = dates[j][i]
        client = MongoClient()
        db = client['pyautodiag']
        collection = db['equipments']
        collection.insert(data,check_keys=False)


def struct2db(FOLDER_PATH):
    """
    Parameters
    ----------
    FOLDER_PATH : str
        String containing the path to the measurements database text folder.

    Returns
    -------
        Populate the MongoDB measurements database.
    """
    counter = 0
    size = len(glob.glob(FOLDER_PATH))
    
    for i in glob.glob(FOLDER_PATH):
        client,area,equipments,POMs,dates = struct2lst(i)
        #lst2db(client,area,equipments,POMs,dates)
        equip2db(client,area,equipments,POMs,dates)
        
        counter = counter + 1
        print "Importing structure "+ str(counter) + "/" + str(size)
        logging.info(str(counter) + "/" + str(size) + " : " + str(i))


import sys
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import XMLConverter, HTMLConverter, TextConverter
from pdfminer.layout import LAParams
from cStringIO import StringIO

import sys
import os
from binascii import b2a_hex


#SOURCE : "http://denis.papathanasiou.org/posts/2010.08.04.post.html"
def save_image (lt_image, page_number, images_folder):
    """Try to save the image data from this LTImage object, and return the file name, if successful"""
    result = None
    if lt_image.stream:
        file_stream = lt_image.stream.get_rawdata()
        file_ext = determine_image_type(file_stream[0:4])
        if file_ext:
            file_name = ''.join([str(page_number), '_', lt_image.name, file_ext])
            if write_file(images_folder, file_name, lt_image.stream.get_rawdata(), flags='wb'):
                result = file_name
    return result

def determine_image_type (stream_first_4_bytes):
    """Find out the image file type based on the magic number comparison of the first 4 (or 2) bytes"""
    file_type = None
    bytes_as_hex = b2a_hex(stream_first_4_bytes)
    if bytes_as_hex.startswith('ffd8'):
        file_type = '.jpeg'
    elif bytes_as_hex == '89504e47':
        file_type = ',png'
    elif bytes_as_hex == '47494638':
        file_type = '.gif'
    elif bytes_as_hex.startswith('424d'):
        file_type = '.bmp'
    return file_type

def write_file (folder, filename, filedata, flags='w'):
    """Write the file data to the folder and filename combination
    (flags: 'w' for write text, 'wb' for write binary, use 'a' instead of 'w' for append)"""
    result = False
    if os.path.isdir(folder):
        try:
            file_obj = open(os.path.join(folder, filename), flags)
            file_obj.write(filedata)
            file_obj.close()
            result = True
        except IOError:
            pass
    return result


def reportInit():
    customer = None
    area = None
    equipment = None
    date = None
    title = None
    urgency = None
    explanation = None
    img = None
    return customer,area,equipment,date,title,\
    urgency,explanation,img


from pyautodiag.config import faults as flt
from pyautodiag.config import urgency as urg
#import pytesser as ocr
import re
from PIL import Image


def ocrPOMextraction(PATH,IMG_PATH):
    """
    """
    os.chdir(r"C:\1_CERISIC\AUTODIAG\Code\pyautodiag\core\db")
    import pytesser as ocr
    image_file = os.path.join(PATH,IMG_PATH)
    
    try:
        img = Image.open(image_file)
        
        # Detect horizontal gray line
        matA = np.asarray(img.crop((0,0,1,img.size[1])).convert('1'))
        idxA = np.where(matA==False)
        
        # If no horizontal line is detected
        if idxA[0].size == 0:
            # Detect date on the right
            idx_tmp = []
            tmp = int(img.size[0]*0.9)
            for i in range(tmp,img.size[0]-1,1):
                matB = np.asarray(img.crop((i,0,i+1,img.size[1])).convert('1'))
                A = np.where(matB==False)
                if A[0].size != 0:
                    idx_tmp.append(np.min(A[0]))
            idxB = np.min(idx_tmp)
            # Detect plot upper border
            matC = np.asarray(img.crop((int(0.25*img.size[0]),0,int(0.25*img.size[0])+1,img.size[1])).convert('1'))
            idx_tmp = np.where(matC==False)
            idxC = np.min(idx_tmp[0])
            
            idx = np.minimum(idxB,idxC)
        else:
            idx = idxA[0][0]
        # Crop image in order to reduce the computation time of the OCR
        cropped_img = img.crop((0,0,img.size[0],idx-2))
        
        # Resize image and convert image to RGB
        resized_img = cropped_img.resize((cropped_img.size[0]*int(round(100.0/cropped_img.size[1])),cropped_img.size[1]*int(round(100.0/cropped_img.size[1]))),Image.BICUBIC)
        RGB_img = resized_img.convert('RGB')
        
        # OCR on the title of the image
        title = ocr.image_to_string(RGB_img)
        
        # Use REGEX to find a string in the title
        m = re.search("[ABCD][VHA][VAP]",title)
    
    except:
        print "No such file"
        m = None
    # Return search result only if a result is found
    if m == None:
        return "???"
    else:
        return m.group()


def faults_detect(title,urgency,area,img):
    """
    """
    
    PATH = r'C:\1_CERISIC\AUTODIAG\Code\pyautodiag\data\Reports\img'
    
    # Set urgency level
    for key in urg.URGENCY.keys():
        if urgency in urg.URGENCY[key]:
            level = key
    
    # Define POMs
    POM = []
    fault_detection = dict()
    if level != "NORMAL":
        
        for i in range(len(img)):
            IMG_PATH = os.path.join(area,img[i])
            POM.append(ocrPOMextraction(PATH,IMG_PATH))
        
        # Define faults
        for fault in flt.FAULTS.keys():
            for j in range(len(flt.FAULTS[fault])):
                m = re.search(flt.FAULTS[fault][j],title.lower())
                if m != None:
                    fault_detection[fault] = dict()
                    for k in range(len(POM)):
                        fault_detection[fault].update({POM[k]:level})
    else: # level == "NORMAL"
        fault_detection["NO_FAULT"] = dict()
        fault_detection["NO_FAULT"].update({"???":level})
    
    return fault_detection



def report2db(PATH,FILENAME):
    """
    """
    os.chdir(PATH)
    report_counter = 0
    
    MONTHS = ["janv.","fev.","mars","avr.","mai","juin","juil.","aout","sept.","oct.","nov.","dec."]
    MONTHS_DEC = ["01","02","03","04","05","06","07","08","09","10","11","12"]
    FILE_EXTENSION = ["jpg","JPG","bmp","BMP","png"]
    
    [customer,area,equipment,date,title,\
    urgency,explanation,img] = reportInit()
    equipment = []
    
    # Connect to MongoDB
    client = MongoClient()
    db = client['pyautodiag']
    reports = db.reports
    
    with open(FILENAME,"r") as f:
        for line in f:
            #line = line.decode('cp1252').encode('utf-8')
            if "Area" and "Urgency:" in line:
                if equipment != [] and date != None:
                    report_counter += 1
                    print "Exporting report " + str(report_counter)
                    # Create JSON object
                    new_report = {"customer":"GSK",
                                "area":area,
                                "SAP":SAP,
                                "equipment":equipment,
                                "fault":fault,
                                "faults":faults_detect(title,urgency,area,img),
                                "date": datetime.datetime(YEAR,MONTH,DAY),
                                "title":title,
                                "urgency":urgency,
                                "explanation":explanation,
                                "img":img
                               }
                    reports.insert_one(new_report)
                    #Reset image list
                    [customer,area,equipment,date,title,\
                    urgency,explanation,img] = reportInit()
                
                area = line[12:].split(" ")[0]
                line = f.next()
                SAP = line[12:].split(" ")[0]
                equipment = " ".join(line[12:].split(" ")[1:]).split("   ")[0]
                urgency = " ".join(line[12:].split(" ")[1:]).strip().split("   ")[-1].strip()
            
            if "Fault:" in line:
                fault = line[12:].split("   ")[0].strip()
            
            if "Title:" in line:
                date = line.split("rvey:")[1].strip()
                DAY = int(date.split("/")[0])
                MONTH = int(date.split("/")[1])
                YEAR = int("20" + date.split("/")[2])
                
                title = []
                if line.split("-")[1] in MONTHS:
                    title.append(line.split(" - ")[1].split("rvey:")[0].strip())
                else:
                    title.append(line[22:].split("rvey:")[0].strip())
                line = f.next()
                while "osed:" not in line:
                    title.append(line.strip())
                    line = f.next()
                title = " ".join(title)
#                report['faults'] = dict()
            if "nation:" in line:
                explanation = []
                while "File:" not in line:
                    explanation.append(line[12:].strip())
                    line = f.next()
                explanation = " ".join(explanation)
                
            if "File:" in line:
                img = []
                tmp = []
                try:
                    if line.split("File:")[1].strip().split(".")[1] in FILE_EXTENSION:
                        tmp = line.split("File:")[1].strip()
                except:
                    line = f.next()
                    while "\\" in line:
                        tmp.append(line.strip())
                        line = f.next()
                    tmp = "\\".join(tmp)
                    tmp = tmp.split("\\")[-1]
                img.append(tmp)
    report_counter += 1
    print "Exporting report " + str(report_counter)
    # Create JSON object
    new_report = {"customer": customer,
                "area": area,
                "SAP":SAP,
                "equipment": equipment,
                "fault": fault,
                "faults":dict(),
                "date": datetime.datetime(YEAR,MONTH,DAY),
                "title":title,
                "urgency":urgency,
                "explanation":explanation,
                "img": img
               }
    reports.insert_one(new_report)




def pdfreport2db(PATH,FILENAME):
    """
    """
    os.chdir(PATH)
    FILEPATH = os.path.join(PATH,FILENAME)
    
    MONTHS = ["janv.","fev.","mars","avr.","mai","juin","juil.","aout","sept.","oct.","nov.","dec."]
    MONTHS_DEC = ["01","02","03","04","05","06","07","08","09","10","11","12"]
    FILE_EXTENSION = ["jpg","JPG","bmp","BMP"]
    
    fp = file(FILEPATH, 'rb')
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = XMLConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    # Create a PDF interpreter object.
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    # Process each page contained in the document.
    
    page_counter = 0
    report_counter = 0
    for page in PDFPage.get_pages(fp):
        page_counter +=1
        logging.info("Processing PDF page "+ str(page_counter))
        print "Processing PDF page "+ str(page_counter)
        interpreter.process_page(page)
        data = retstr.getvalue()
        logging.info("Writing page "+ str(page_counter) + " to text file")
        print "Writing page "+ str(page_counter) + " to text file"
        with open("test.txt", "w") as f:
            f.write(data)
        f.close()
    
    # Connect to MongoDB
    client = MongoClient()
    db = client['pyautodiag']
    reports = db.reports
    equipment = None
    img = date = customer = area = SAP = title = urgency = explanation = []
    with open(FILENAME,"r") as f:
        for line in f:
            if "Equipment" in line:
                if equipment != [] and date != None:
                    report_counter += 1
                    print "Exporting report " + str(report_counter)
                    # Create JSON object
                    new_report = {"customer": customer,
                                "area": area,
                                "SAP":SAP,
                                "equipment": equipment,
                                "date": datetime.datetime(YEAR,MONTH,DAY),
                                "title":title,
                                "urgency":urgency,
                                "explanation":explanation,
                                "img": img
                               }
                    reports.insert_one(new_report)
                    #Reset image list
                    [customer,area,equipment,date,title,\
                    urgency,explanation,img] = reportInit()
                try:
                    line = f.next()
                    line = f.next()
                    line = f.next()
                    
                    #equipment = line.strip()
                    SAP = line.strip().split(" ")[0]
                    equipment = " ".join(line.strip().split(" ")[1:])
                    
                    line = f.next()
                    line = f.next()
                    
                    try:
                        customer = line.strip().split("-")[0]
                        area = line.strip().split("-")[1]
                    except:
                        print "Exception catched"
                except:
                    print "Exception catched"
            if "/" in line and "-" in line:
                try:
                    date = line.strip().split("-")[0].strip()
                    DAY = int(date.split("/")[0])
                    MONTH = int(date.split("/")[1])
                    YEAR = int("20" + date.split("/")[2])
                    title = "-".join(line.strip().split("-")[1:]).strip()
                except:
                    print "Exception catched"
            
            if "Explanation:" in line:
                line = f.next()
                line = f.next()
                explanation = []
                while line.strip().split("/")[0] not in MONTHS_DEC:
                    explanation.append(line.strip())
                    line = f.next()
                explanation = " ".join(explanation)
            if "Urgency:" in line:
                line = f.next()
                urgency = line.strip()
            if "File:" in line:
                img = []
                img.append(line.split(":")[1].strip())



if __name__== "__main__":
    
    
#    FILE_PATH = 'C:\\Users\\JVA\\Google Drive\\Personal\\AutoDiag\\Code\\pyautodiag\\extract\\tmpFile.txt'
    
#    txt2db(FILE_PATH)
    
    
    FILE_PATH = r'C:\1_CERISIC\AUTODIAG\Code\pyautodiag\data\AUTODIAG\GSK\WN10\EC10.12_ZONE 1\AVA-20150130-10h15.txt'
    meas = load_measurement(FILE_PATH, output='json')
    meas = json.loads(meas, object_hook=json_util.object_hook)
    
    PATH = r'C:\1_CERISIC\AUTODIAG\Code\pyautodiag\data\Reports'
    #FILENAME = "Reports-WN10.txt"
    FILENAME = "AUTODIAG-Reports.pdf"
    FULL_PATH = os.path.join(PATH,FILENAME)
    report2db(FULL_PATH)
    #pdf2txt(FULL_PATH)
    #report2lst(FULL_PATH)
